package dr.server;

import java.io.File;

import org.junit.Test;

public class EraseDb {

	@Test
	public void test() {
		erase("DRDBTEST.txt");
		erase("DRDBTEST2.txt");
		erase("DRDBTEST3.txt");
		erase("DRDBTEST4.txt");
		erase("DRDBTEST5.txt");
		erase("DRDBTEST6.txt");
		erase("DRDBTEST7.txt");
		erase("DRDBTEST8.txt");
		erase("DRDBTEST9.txt");
	}

	public void erase(String toErase){
		File f = new File( toErase );
		f.delete();
		File p = new File( toErase+".p" );
		p.delete();
		File t = new File( toErase+".t" );
		t.delete();
	}
}
