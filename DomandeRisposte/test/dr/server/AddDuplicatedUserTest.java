package dr.server;
import java.io.File;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import dr.shared.DuplicatedUserNameException;
import junit.framework.TestCase;

public class AddDuplicatedUserTest extends TestCase {
	DB db = DBMaker.newFileDB(new File("DRDBTEST3.txt"))
	        .closeOnJvmShutdown()
	      	.make();
	LoginServiceImpl service = new LoginServiceImpl( db );
	
	public void setUp(){
		service.remove( "Franco" );
	}
	
	public void testAddTwoUser() throws DuplicatedUserNameException{		
		String u1_username = "Franco";
		String u1_password = "password";
		String u1_email ="franco@mail.it";

		String u2_username = "Franco";
		String u2_password = "unapassword";
		String u2_email = "franchello@mail.it";
		
		boolean exceptionThrown = false;
		
		service.addUser(u1_username, u1_password, u1_email, null, null, null, null, null, null);
		
		try{
			service.addUser(u2_username, u2_password, u2_email, null, null, null, null, null, null);
		}
		catch( DuplicatedUserNameException e){
			exceptionThrown = true;
		}
		assertTrue( exceptionThrown );
	}
	
}
