package dr.server;
import java.io.File;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import dr.shared.DuplicatedUserNameException;
import dr.shared.User;
import dr.shared.UserNotFoundException;
import dr.shared.WrongPasswordException;
import junit.framework.TestCase;

public class NominateJudgeTest extends TestCase {
	
	DB db = DBMaker.newFileDB( new File( "DRDBTEST9.txt" ) ).closeOnJvmShutdown().make();
	
	LoginServiceImpl service = new LoginServiceImpl( db );
	
	public void testNomination() throws DuplicatedUserNameException{
		service.remove( "Bruce" );
		String userName = "Bruce";
		String password = "Wayne";
		String email = "bruce@wayne.bat";

		User user = service.addUser(userName, password, email, null, null, null, null, null, null);
		
		assertTrue( user.getRole() == 'r');
		
		service.nominateJudge( userName );
		
		try {
			user = service.login( userName , password );
		} catch ( WrongPasswordException e ) {
			e.printStackTrace();
		} catch ( UserNotFoundException e ) {
			e.printStackTrace();
		}
		assertTrue( user.getRole() == 'j');
	}
}
