package dr.server;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;

import org.mapdb.DB;
import org.mapdb.DBMaker;

import dr.shared.Answer;
import junit.framework.TestCase;

public class AddAnswerTest extends TestCase {
	DB db = DBMaker.newFileDB( new File( "DRDBTEST2.txt" ) ).closeOnJvmShutdown().make();
	QuestionServiceImpl service = new QuestionServiceImpl( db );
	CategoryServiceImpl catService = new CategoryServiceImpl( db );
	
	public void setUp(){
		service.clear();
		catService.clear();
		catService.init();
	}
	
	public void testAddAnswer(){
		// Aggiungo 2 domande
		String text = "Che differenza c'è tra java e JavaScript?";
		String author = "Noobo";
		int categoryId = 2;
		
		String text2 = "Come si fa a far venire quelli di YouTube a filmarti? Ho provato provato provato a chiamare YouTube tutto il giorno perché venissero a filmarmi ma non mi hanno risposto. Come fanno le altre persone ad avere lì i loro video? Avrei della roba divertente ma non vengono.";
		String author2 = "R. Brunetta";
		int categoryId2 = 3;
		
		service.addQuestion(text, author, categoryId); // aggiunta con id 0
		service.addQuestion(text2, author2, categoryId2); // aggiunta con id 1
		
		// Aggiungo 2 risposte
		service.addAnswer( "Sono 2 linguaggi diversi", 0, "Philip J. Fry", new Date() );
		service.addAnswer( "Prova con una mail", 1, "Philip J. Fry", new Date() );
		
		boolean added1 = false;
		boolean added2 = false;
		
		// Controllo se le risposte sono state inserite correttamente nell array di answer della Question
		ArrayList<Answer> ans0 = service.getFullQuestion( 0 ).getAnswers();
		for( Answer a : ans0 ){
			if( a.getText().equals( "Sono 2 linguaggi diversi" )){
				added1 = true;
			}
		}
		
		ArrayList<Answer> ans1 = service.getFullQuestion( 1 ).getAnswers();
		for( Answer a : ans1 ){
			if(a.getText().equals( "Prova con una mail" )){
				added2 = true;
			}
		}
		
		assertTrue( added1 && added2 );
	}
}

