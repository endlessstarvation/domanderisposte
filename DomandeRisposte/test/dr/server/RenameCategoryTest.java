package dr.server;
import java.io.File;
import org.junit.Test;
import org.mapdb.DB;
import org.mapdb.DBMaker;

import junit.framework.TestCase;

public class RenameCategoryTest extends TestCase{
	
	
	
	
	DB db = DBMaker.newFileDB(new File("DRDBTEST8.txt"))
	        .closeOnJvmShutdown()
	      	.make();
	CategoryServiceImpl service = new CategoryServiceImpl( db );
	
	public void setUp(){
		service.clear();
		service.init();
	}
	
	
	@Test
	public void testRename() {
		service.addCategory("Example");
		service.addCategory(1, "Ecologia");
		service.addCategory( 4, "Programmazione");
		service.addCategory( 9, "JavaScript");
		service.addCategory( 9, "Java");
		service.addCategory( 9, "PHP");
		// La categoria php viene aggiunta con id 12
		// True che il nome è "PHP"
		assertTrue( service.getCategory( 12 ).getName().equals( "PHP" ));
		
		service.renameCategory( 12, "Python");
		
		// Adesso il nome della cat con id 12 è Python
		assertTrue( service.getCategory( 12 ).getName().equals( "Python" ));

	}

}
