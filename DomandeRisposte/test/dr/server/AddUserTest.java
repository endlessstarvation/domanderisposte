package dr.server;
import java.io.File;

import org.mapdb.DB;
import org.mapdb.DBMaker;

import dr.shared.DuplicatedUserNameException;
import dr.shared.User;
import junit.framework.TestCase;

public class AddUserTest extends TestCase {
	
	DB db = DBMaker.newFileDB( new File( "DRDBTEST4.txt" ) ).closeOnJvmShutdown().make();
	
	LoginServiceImpl service = new LoginServiceImpl( db );
	
	public void testAddAUser() throws DuplicatedUserNameException{
		service.remove( "Bruce" );
		String userName = "Bruce";
		String password = "Wayne";
		String email = "bruce@wayne.bat";

		User user = service.addUser(userName, password, email, null, null, null, null, null, null);
		assertTrue( user.getUsername().equals( userName ));
		assertTrue( user.getPassword().equals( password ));
		assertTrue( user.getEmail().equals(email));
	}
	
	public void testAddCompleteUser()throws DuplicatedUserNameException{
		service.remove( "Fry" );
		String userName = "Fry";
		String password = "leila";
		String email = "philip@jFry.fut";
		String name = "Philip J.";
		String surname = "Fry";
		String sex = "male";
		String birthDate = "9/8/1974";
		String birthPlace = "New York";
		String adress = "57th Street Manhattan";
		
		User user = service.addUser(userName, password, email, name, surname, sex, birthDate, birthPlace, adress);
		assertTrue( user.getUsername().equals(userName));
		assertTrue( user.getPassword().equals(password));
		assertTrue( user.getEmail().equals(email));
	}
}
