package dr.server;
import java.io.File;
import java.util.ArrayList;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import dr.shared.Question;
import junit.framework.TestCase;

public class AddQuestionTest extends TestCase {
	DB db = DBMaker.newFileDB( new File( "DRDBTEST5.txt" ) ).closeOnJvmShutdown().make();
	QuestionServiceImpl service = new QuestionServiceImpl( db );
	CategoryServiceImpl catService = new CategoryServiceImpl( db );
	
	public void setUp(){
		service.clear();
		catService.clear();
		catService.init();
	}
	
	public void testAddQuestion(){
		String text = "Che differenza c'è tra java e JavaScript?";
		String author = "Noobo";
		int categoryId = 2;
		
		String text2 = "Come si fa a far venire quelli di YouTube a filmarti? Ho provato provato provato a chiamare YouTube tutto il giorno perché venissero a filmarmi ma non mi hanno risposto. Come fanno le altre persone ad avere lì i loro video? Avrei della roba divertente ma non vengono.";
		String author2 = "R. Brunetta";
		int categoryId2 = 3;
		
		service.addQuestion(text, author, categoryId);
		
		service.addQuestion(text2, author2, categoryId2);
		
		ArrayList<Question> qs = service.getAllQuestions();
		
		boolean added1 = false;
		boolean added2 = false;
		
		for( int i = 0; i < qs.size(); i++ ){
			Question q = qs.get( i );
			
			if( q.getText().equals( text ) ) added1 = true;
			if( q.getText().equals( text2 ) ) added2 = true;
		}
		assertTrue( added1 && added2 );
	}
}
