package dr.server;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
  EraseDb.class,
  AddAnswerTest.class,
  AddDuplicatedUserTest.class,
  AddQuestionTest.class,
  AddUserTest.class,
  getCategoriesTreeTest.class,
  LoginTest.class,
  NominateJudgeTest.class,
  newCategoryTest.class,
  RenameCategoryTest.class,
  EraseDb.class,
})

public class SuiteTest {
  // used only as a holder for the above annotations
}
