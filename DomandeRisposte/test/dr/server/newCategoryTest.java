package dr.server;
import java.io.File;

import org.junit.Test;
import org.mapdb.DB;
import org.mapdb.DBMaker;

import dr.shared.CategoryExt;

import junit.framework.TestCase;

public class newCategoryTest extends TestCase {
	DB db = DBMaker.newFileDB(new File("DRDBTEST8.txt"))
	        .closeOnJvmShutdown()
	      	.make();
	
	
	CategoryServiceImpl service = new CategoryServiceImpl( db );
	
	public void setUp(){
		service.clear();
		service.init();
	}
	
	@Test
	public void testAddCategory(){
		String name = "ExampleCategory";
		service.addCategory( name );
		CategoryExt root = service.getCategoryRoot();
		CategoryExt addedCat = null;
		boolean addedToFatherSubcategories = false;
		for( int i = 0; i < root.getSubcategoriesIds().size(); i++ ){
			addedCat = service.getCategory( root.getSubcategoriesIds().get( i ));
			System.out.println( addedCat.getName() + " - " + addedCat.getId()+" - "+addedCat.getFatherId() );
			if( addedCat.getName().equals(name) ){
				addedToFatherSubcategories = true;
				//break;
			}
		}
		// redundant, second assertion implies the first.
		assertTrue( addedCat != null);
		assertTrue( addedToFatherSubcategories );
	}
	
	public void testAddChildCategory(){
		String name = "Programmazione";
		service.addCategory( 4, name);
		service.addCategory( 4, "Mobile");
		boolean added = false;
		CategoryExt cat = null;
		CategoryExt tecno = service.getCategory( 4 );
		for( int id : tecno.getSubcategoriesIds()){
			cat = service.categoriesExt.get( id );
			System.out.println( cat.getName()+" - "+cat.getId()+" - "+cat.getFatherId());
			if( cat.getName().equals("Programmazione")){
				added = true;
			}
		}
		assertTrue( added ); 
		
	}	
}
