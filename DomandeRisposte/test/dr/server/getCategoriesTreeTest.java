package dr.server;
import java.io.File;
import org.junit.Test;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import dr.shared.Category;
import junit.framework.TestCase;

public class getCategoriesTreeTest extends TestCase {
	DB db = DBMaker.newFileDB(new File("DRDBTEST6.txt"))
	        .closeOnJvmShutdown()
	      	.make();
	CategoryServiceImpl service = new CategoryServiceImpl( db );
	
	public void setUp(){
		service.clear();
		service.init();
	}
	
	@Test
	public void testgetCat(){
		service.addCategory("Example");
		service.addCategory(1, "Ecologia");
		service.addCategory( 4, "Programmazione");
		service.addCategory( 9, "JavaScript");
		service.addCategory( 9, "Java");
		service.addCategory( 9, "PHP");
		//service.renameCategory( 12, "Python");
		Category root = service.getCategoryTree();
		printSubs("", root);
	}
	
	public void printSubs(String space, Category root) {
		if (root.getSubcategories() != null) {
			for (int i = 0; i < root.getSubcategories().size(); i++) {
				Category sub = root.getSubcategories().get(i);
				System.out
				.println(space + " - " + sub.getName() + " - " + sub.getId() + " - " + sub.getFather().getId());
				printSubs(space + "   ", sub);
			}
		}
	}
}
