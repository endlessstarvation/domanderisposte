package dr.server;
import java.io.File;

import org.mapdb.DB;
import org.mapdb.DBMaker;

import dr.shared.DuplicatedUserNameException;
import dr.shared.User;
import dr.shared.UserNotFoundException;
import dr.shared.WrongPasswordException;
import junit.framework.TestCase;


public class LoginTest extends TestCase {
	DB db = DBMaker.newFileDB( new File( "DRDBTEST7.txt" ) ).closeOnJvmShutdown().make();
	LoginServiceImpl service = new LoginServiceImpl( db );
	
	public void setUp(){
		service.clear();
	}
	
	public void testLogin()throws WrongPasswordException, UserNotFoundException {
		String userName = "Bruce";
		String password = "Wayne";
		String email = "bruce@wayne.bat";
		
		try {
			service.addUser( userName , password , email , null , null , null , null , null , null );
		} catch ( DuplicatedUserNameException e ) {
			e.printStackTrace();
		}
		
		User user = service.login( userName, password);
		
		assertTrue( user.getUsername().equals(userName));
		assertTrue( user.getPassword().equals(password));
		assertTrue( user.getEmail().equals( email ));	
	}
	
	public void testLoginComplete()throws WrongPasswordException, UserNotFoundException {
		String userName = "Fry";
		String password = "leela";
		String email = "philip@jFry.fut";
		String name = "Philip J.";
		String surname = "Fry";
		String sex = "male";
		String birthDate = "13/11/1969";
		String birthPlace = "New York";
		String address = "57th Street Manhattan";
		
		try {
			service.addUser( userName , password , email , name , surname , sex , birthDate , birthPlace , address );
		} catch ( DuplicatedUserNameException e ) {
			e.printStackTrace();
		}
		
		User user = service.login( userName, password);
		assertEquals( user.getUsername(), userName);
		assertEquals( user.getPassword(), password);
		assertEquals( user.getEmail(), email);
		assertEquals( user.getName(), name);
		assertEquals( user.getSurname(), surname);
		assertEquals( user.getSex(), sex);
		assertEquals( user.getBirthDate(), birthDate);
		assertEquals( user.getBirthPlace(), birthPlace);
		assertEquals( user.getAddress(), address);
	}
}
