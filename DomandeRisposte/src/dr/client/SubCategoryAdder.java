package dr.client;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

/**
 * Classe che gestisce la sezione dell'interfaccia grafica dell'Amministratore per
 * l'aggiunta di una nuova sotto categoria
 */
public class SubCategoryAdder extends Composite
{
  interface SubCategoryAdderBinder extends UiBinder<Widget, SubCategoryAdder> {}
  private static SubCategoryAdderBinder uiBinder = GWT.create(SubCategoryAdderBinder.class);
  
  private CategoryManagement owner;

  @UiField Label title;
  @UiField ListBox father;
  @UiField TextBox newName;
  @UiField Button add;
  
  public SubCategoryAdder(CategoryManagement owner, List<String[]> categoryNameList)
  {
    this.owner = owner;
    
    initWidget(uiBinder.createAndBindUi(this));
    
    for(String[] s:categoryNameList)
    {
      father.addItem(s[1]);
    }
    
    title.getElement().setId("label");
    father.setStyleName("managerItem");
    newName.setStyleName("managerItem");
    add.setStyleName("managerItem");
    this.setStyleName("managerDiv");
  }
  
  /*
   * Associo il clickHandler al Button attraverso l'UiHandler:
   * Se stati specificati il nome della sottocategoria e la categoria padre, allora viene richiesta l'aggiunta della nuova sotto categoria
   * Se non è stato specificato il nome della sottocategoria o della categoria padre, allora viene avvertito l'utente del problema
   */
  @UiHandler("add")
  void handleClick(ClickEvent event) {
    if(father.getSelectedIndex() != 0)
    {
      if(!newName.getText().equals(""))
      {
        owner.addNewSubCategory(father.getSelectedIndex(), newName.getText());
        father.setSelectedIndex(0);
        newName.setText("");
      } else
      {
        owner.alertFailure("Specificare il nome della nuova sotto categoria");
      }
    } else
    {
      owner.alertFailure("Specificare la categoria di appartenenza della sotto categoria");
    }
  }
}
