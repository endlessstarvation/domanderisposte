package dr.client;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.user.client.rpc.AsyncCallback;

import dr.shared.Category;
import dr.shared.Question;
import dr.shared.QuestionExtended;
import dr.shared.User;

public interface DRServiceAsync {

	void login(String userName, String password, AsyncCallback<User> callback);

	void addUser(String username, String password, String email, String name, String surname, String sex,
			String birthDate, String birthPlace, String address, AsyncCallback<User> callback);

	void addQuestion(String text, String author, int categoryId, AsyncCallback<ArrayList<Question>> callback);

	void getAllQuestions(AsyncCallback<ArrayList<Question>> callback);
	
	void getFullQuestion(int questionId, AsyncCallback<QuestionExtended> callback);
	
	void getAllQuestions(int firstKey, AsyncCallback<ArrayList<Question>> callback);

	void getAllQuestionsByCategory(int categoryId, AsyncCallback<ArrayList<Question>> callback);

	void addAnswer(String text, int questionId, String author, Date submissionDate, AsyncCallback<QuestionExtended> callback);

	void removeQuestion(int questionId, AsyncCallback<ArrayList<Question>> callback);
	
	void removeAnswer( int questionId, int answerId, AsyncCallback<QuestionExtended> callback);

  void getCategoryTree(AsyncCallback<Category> callback);

	void addJudgement(int questionId, int answerId, String author, int val, AsyncCallback<QuestionExtended> callback);

	void getAllUsers(AsyncCallback<ArrayList<User>> callback);

	void getJudges( AsyncCallback<ArrayList<User>> callback );

	void getRegistered( AsyncCallback<ArrayList<User>> callback );

	void nominateJudge( String userName, AsyncCallback<ArrayList<User>> callback );
	
	void addCategory( String name, AsyncCallback<Category> callback );

  void addSubCategory( int fatherId, String name, AsyncCallback<Category> callback );

  void renameCategory( int categoryId, String newName, AsyncCallback<Category> callback );
}
