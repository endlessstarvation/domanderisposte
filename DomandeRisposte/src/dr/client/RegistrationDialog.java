package dr.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

/**
 * Classe che gestisce la sezione dell'interfaccia grafica per
 * la registrazione di un nuovo utente
 */
public class RegistrationDialog extends DialogBox
{
  interface RegistrationBinder extends UiBinder<Widget, RegistrationDialog> {}
  private static RegistrationBinder uiBinder = GWT.create(RegistrationBinder.class);
  
  @UiField TextBox username;
  @UiField PasswordTextBox password;
  @UiField TextBox email;
  @UiField TextBox name;
  @UiField TextBox lastname;
  @UiField RadioButton m;
  @UiField RadioButton f;
  @UiField DateBox birthdate;
  @UiField TextBox birthplace;
  @UiField TextBox address;
  @UiField Button registration;
  @UiField Button cancel;
  
  private Controller controller;
  
  public RegistrationDialog(Controller controller) 
  {
    this.controller = controller;
    
    setWidget(uiBinder.createAndBindUi(this));
    
    // Impostazione del formato della data visualizzata dal DateBox
    DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd/MM/yyyy");
    birthdate.setFormat(new DateBox.DefaultFormat(dateFormat));
    birthdate.addStyleName("datePicker");

    // Impostazione del placeholder dei Textfield
    username.getElement().setPropertyString("placeholder", "Inserisci il tuo username");
    password.getElement().setPropertyString("placeholder", "Inserisci la tua password");
    email.getElement().setPropertyString("placeholder", "Inserisci la tua email");
    name.getElement().setPropertyString("placeholder", "Inserisci il tuo nome");
    lastname.getElement().setPropertyString("placeholder", "Inserisci il tuo cognome");
    birthdate.getElement().setPropertyString("placeholder", "Seleziona la tua data di nascita");
    birthplace.getElement().setPropertyString("placeholder", "Inserisci il tuo luogo di nascita");
    address.getElement().setPropertyString("placeholder", "Inserisci il tuo indirizzo");
    
    this.setText("Registrazione");
    this.center();
    this.show();
  }
  
  /*
   * Associo il clickHandler ai Button attraverso l'UiHandler
   * - registration:
   *    se i TextField obbligatori sono stati correttamente riempiti, viene effettuata una richiesta di registrazione
   *    se i TextField obbligatori non sono stati correttamente riempit, viene avvertito l'utente del problema
   * - cancel: chiude la finestra di registrazione
   */
  @UiHandler(value={"registration", "cancel"})
  void handleClick(ClickEvent event) {
    Widget sender = (Widget) event.getSource();
    if(sender == registration)
    {
      if(!isEmpty())
      {
        if(isValidEmail(email.getText()))
        {
          controller.registration
            (
              username.getText(), 
              password.getText(),
              email.getText(),
              name.getText(),
              lastname.getText(),
              gender(),
              birthdate.getTextBox().getText(),
              birthplace.getText(),
              address.getText()
            );
        } else
        {
          new Dialog("alert", "Attenzione formato dell'email non corretto");
        }
      } else
      {
        new Dialog("alert", "Attenzione riempire tutti i campi");
        cleanFields();
      }
    } else if(sender == cancel)
    {
      this.hide();
    }
  }
  
  /*
   * Metodo per impostare il sesso dell'utente a partire dalla scelta effettuata
   */
  private String gender()
  {
    if(m.getValue())
    {
      return "m";
    }
    if(f.getValue())
    {
      return "f";
    }
    return "";
  }
  
  /*
   * Metodo che controlla il corretto riempimento dei TextFields obbligatori
   */
  private boolean isEmpty()
  {
    if(username.getText().isEmpty() || password.getText().isEmpty() || email.getText().isEmpty())
    {
      return true;
    }
    return false;
  }
  
  /*
   * Metodo che controlla che l'email inserita sia in un formato valido
   */
  private boolean isValidEmail(String email)
  {
    return email.matches("^([a-zA-Z0-9_.\\-+])+@(([a-zA-Z0-9\\-])+\\.)+[a-zA-Z0-9]{2,4}$");
  }
  
  /*
   * Metodo che ripulisce i TextField e il DateBox
   */
  public void cleanFields()
  {
    username.setText("");
    password.setText("");
    email.setText("");
    name.setText("");
    lastname.setText("");
    m.setValue(false);
    f.setValue(false);
    birthdate.getTextBox().setText("");
    birthplace.setText("");
    address.setText("");
  }
  
  /*
   * Metodo che setta il focus sul TextField username
   */
  @Override
  public void show()
  {
    super.show();

    Scheduler.get().scheduleDeferred(new Command()
    {
      public void execute()
      {
        username.setFocus(true);
      }
    });
  }
}
