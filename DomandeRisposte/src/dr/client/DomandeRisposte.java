package dr.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;

/**
 * Classe che gestisce l'avvio dell'applicazione
 */
public class DomandeRisposte implements EntryPoint
{
  private Controller controller = GWT.create(Controller.class);
  @Override
  public void onModuleLoad()
  {
    controller.init();
  }
}


