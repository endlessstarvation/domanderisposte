package dr.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.Widget;

/**
 * Classe che gestisce la sezione dell'interfaccia grafica per
 * l'inserimento di una nuova risposta
 */
public class NewAnswer extends Composite
{
  interface NewAnswerBinder extends UiBinder<Widget, NewAnswer> {}
  private static NewAnswerBinder uiBinder = GWT.create(NewAnswerBinder.class);
  
  @UiField TextArea answerText;
  @UiField Button sendAnswer;
  @UiField Button clear;
  
  private QuestionDialog owner;
  
  public NewAnswer(QuestionDialog owner) 
  {
    this.owner = owner;
   
    initWidget(uiBinder.createAndBindUi(this));
    
    answerText.getElement().setPropertyString("placeholder", "Inserisci una risposta...");
    answerText.getElement().setId("answerTextArea");
  }
  
  //Metodo che controlla se è stato inserito un testo per la risposta
  private boolean answerHasText()
  {
    return !answerText.getText().equals("");
  }
  
  //Associo il clickHandler ai Button attraverso l'UiHandler
  @UiHandler(value={"sendAnswer", "clear"})
  void handleClick(ClickEvent event) {
    Widget sender = (Widget) event.getSource();
    if(sender == sendAnswer)
    {
      if(answerHasText())
      {
        owner.addAnswer(answerText.getText());
      } else
      {
        owner.alertFailure("Prego inserire una risposta");
      }
    } else if(sender == clear)
    {
      clearTextArea(); //Viene ripulita la TextArea
    }
  }
  
  //Metodo che ripulisce lo spazio dove viene inserito il testo della risposta
  public void clearTextArea()
  {
    answerText.setText("");
  }
}
