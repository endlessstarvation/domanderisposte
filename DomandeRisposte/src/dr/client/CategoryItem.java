package dr.client;

import com.google.gwt.user.client.ui.TreeItem;

import dr.shared.Category;

public class CategoryItem extends TreeItem{
	
	private int id;
	
	public CategoryItem(Category c){
		if(c != null){
			this.id = c.getId();
			setText(c.getName());
		} else {
			setText("Categorie");
		}
	}
	
	public int getId(){
		return id;
	}
	
}
