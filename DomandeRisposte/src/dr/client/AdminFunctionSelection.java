package dr.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

/**
 * Classe che gestisce la sezione dell'interfaccia grafica dell'Amministratore per
 * la visualizzazione delle finestre di gestione delle categorie e di gestione delle promozioni
 */
public class AdminFunctionSelection extends Composite
{
  interface AdminFunctionSelectionBinder extends UiBinder<Widget, AdminFunctionSelection> {}
  private static AdminFunctionSelectionBinder uiBinder = GWT.create(AdminFunctionSelectionBinder.class);

  @UiField HTMLPanel functionContainer;
  @UiField Label box_title;
  @UiField Button categoryManagement;
  @UiField Button judgesManagement;
  
  private Controller controller;
  
  public AdminFunctionSelection(Controller controller) 
  {
    this.controller = controller;
    
    initWidget(uiBinder.createAndBindUi(this));
    
    box_title.setStyleName("box_title");
    categoryManagement.setStyleName("rightBoxElem");
    judgesManagement.setStyleName("rightBoxElem");
    functionContainer.setStyleName("rightBox");
  }
  
  //Associo il clickHandler ai Button attraverso l'UiHandler
  @UiHandler(value={"categoryManagement", "judgesManagement"})
  void handleClick(ClickEvent event) {
    Widget sender = (Widget) event.getSource();
    if(sender == categoryManagement)
    {
      controller.getManagementCategory(); //Visualizzazione della finestra di gestione delle categorie
    } else if(sender == judgesManagement)
    {
      controller.getRegisteredUsersBoard(); //Visualizzazione della finestra di gestione delle promozioni
    }
  }
}
