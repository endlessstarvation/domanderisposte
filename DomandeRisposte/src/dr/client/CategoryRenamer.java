package dr.client;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

/**
 * Classe che gestisce la sezione dell'interfaccia grafica dell'Amministratore per
 * il cambio di nomen di una categoria
 */
public class CategoryRenamer extends Composite
{
  interface CategoryRenamerBinder extends UiBinder<Widget, CategoryRenamer> {}
  private static CategoryRenamerBinder uiBinder = GWT.create(CategoryRenamerBinder.class);
  
  private CategoryManagement owner;

  @UiField Label title;
  @UiField ListBox categoryList;
  @UiField TextBox newName;
  @UiField Button rename;
  
  public CategoryRenamer(CategoryManagement owner, List<String[]> categoryNameList)
  {
    this.owner = owner;
    
    initWidget(uiBinder.createAndBindUi(this));
    
    for(String[] s:categoryNameList)
    {
      categoryList.addItem(s[1]);
    }
    
    title.getElement().setId("label");
    categoryList.setStyleName("managerItem");
    newName.setStyleName("managerItem");
    rename.setStyleName("managerItem");
    this.setStyleName("managerDiv");
  }
  
  /*
   * Associo il clickHandler al Button attraverso l'UiHandler
   * Se è stata selezionata la categoria da rinominare e ne è stato specificato il nuovo nome
   *  allora viene effettuata la richiesta d'inserimento di una nuova categoria
   * Se non  è stata selezionata la categoria da rinominare o non ne è stato specificato il nuovo nome
   *  allova viene avvertito l'utente 
   */
  @UiHandler("rename")
  void handleClick(ClickEvent event) {
    if(categoryList.getSelectedIndex() != 0)
    {
      if(!newName.getText().equals(""))
      {
        //Il nuovo nome della categoria non può essere uguale a quello attuale
        if(!checkSameName(newName.getText(), categoryList.getItemText(categoryList.getSelectedIndex())))
        {
          owner.renameCategory(categoryList.getSelectedIndex(), newName.getText());
          categoryList.setSelectedIndex(0);
          newName.setText("");
        } else
        {
          newName.setText("");
          owner.alertFailure("Il nuovo nome della categoria non può essere ugale a quello attuale");
        }
      } else
      {
        owner.alertFailure("Specificare il nuovo nome della categoria");
      }
    } else
    {
      owner.alertFailure("Specificare la categoria da rinominare");
    }  
  }
  
  //Metodo che controlla che il nuovo nome della categoria sia differente da quello attuale
  private boolean checkSameName(String newName, String currentName)
  {
    String[] splitName = currentName.split("-");
    return splitName[splitName.length-1].trim().toLowerCase().equals(newName.toLowerCase());
  }
}
