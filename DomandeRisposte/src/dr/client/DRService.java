package dr.client;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import dr.shared.Category;
import dr.shared.DuplicatedUserNameException;
import dr.shared.Question;
import dr.shared.QuestionExtended;
import dr.shared.User;
import dr.shared.UserNotFoundException;
import dr.shared.WrongPasswordException;

@RemoteServiceRelativePath("DomandeRisposte")
public interface DRService extends RemoteService{
	
	User addUser( String username, String password, String email, String name, String surname, String sex, String birthDate, String birthPlace, String address )throws DuplicatedUserNameException;
	
	User login( String userName, String password)throws WrongPasswordException, UserNotFoundException;
	
	ArrayList<Question>addQuestion( String text, String author, int categoryId );
	
	ArrayList<Question>getAllQuestions();
	
	ArrayList<Question>getAllQuestions( int firstKey );
	
	ArrayList<User> getAllUsers();
	
	ArrayList<User> getJudges();
	
	ArrayList<User> getRegistered();
	
	ArrayList<User> nominateJudge( String userName );
	
	QuestionExtended getFullQuestion(int questionId);

	ArrayList<Question>getAllQuestionsByCategory( int categoryId );
	
	QuestionExtended addAnswer( String text, int questionId, String author, Date submissionDate );

	ArrayList<Question> removeQuestion( int questionId);
	
	QuestionExtended removeAnswer( int questionId, int answerId );
	
	QuestionExtended addJudgement( int questionId, int answerId, String author, int val);
	
	Category getCategoryTree();
	
	Category addCategory(String name);
  
	Category addSubCategory(int fatherId, String name);
  
	Category renameCategory(int categoryId, String newName);
	
}
