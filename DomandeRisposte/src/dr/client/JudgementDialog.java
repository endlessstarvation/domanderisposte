package dr.client;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

import dr.shared.Judgement;

/**
 * Classe che gestisce la sezione dell'interfaccia grafica per
 * la visualizzazionde dell'elenco delle coppie Giudizio-Giudice
 */
public class JudgementDialog extends DialogBox
{
  interface JudgementDialogBinder extends UiBinder<Widget, JudgementDialog> {}
  private static JudgementDialogBinder uiBinder = GWT.create(JudgementDialogBinder.class);
  
  @UiField HTMLPanel judgementsContainer;
  
  public JudgementDialog(ArrayList<Judgement> judgements, int left, int top)
  {
    setWidget(uiBinder.createAndBindUi(this));
    
    for(Judgement j:judgements)
    {
      judgementsContainer.add(new SingleJudgement(j));
    }
    
    this.setAutoHideEnabled(true);
    this.setPopupPosition(left+10, top);
    this.show();
  }
}
