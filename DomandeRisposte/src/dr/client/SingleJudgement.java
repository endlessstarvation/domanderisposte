package dr.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import dr.shared.Judgement;

/**
 * Classe che gestisce la sezione dell'interfaccia grafica relativa
 * alla visualizzazione della coppia Giudizio-Giudice nella finestra della domanda specifica
 */
public class SingleJudgement extends Composite
{
  interface SingleJudgementBinder extends UiBinder<Widget, SingleJudgement> {}
  private static SingleJudgementBinder uiBinder = GWT.create(SingleJudgementBinder.class);
  
  @UiField Label judgement;
  @UiField Label author;
  
  public SingleJudgement(Judgement j)
  {
    initWidget(uiBinder.createAndBindUi(this));
    
    judgement.setText(""+j.getVal());
    author.setText("-"+j.getAuthor());
  }
}
