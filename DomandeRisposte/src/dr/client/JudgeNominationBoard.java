package dr.client;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

import dr.shared.User;

/**
 * Classe che gestisce la sezione dell'interfaccia grafica dell'Amministratore per
 * la visualizzazione della finestra di gestione delle promozioni degli utenti
 */
public class JudgeNominationBoard extends DialogBox
{
  interface JudgeNominationBoardBinder extends UiBinder<Widget, JudgeNominationBoard> {}
  private static JudgeNominationBoardBinder uiBinder = GWT.create(JudgeNominationBoardBinder.class);

  @UiField HTMLPanel registeredUsersListContainer;
  
  private Controller controller;
  
  public JudgeNominationBoard(Controller controller, ArrayList<User> registeredUsersList) 
  {
    this.controller = controller;
    
    setWidget(uiBinder.createAndBindUi(this));
    
    showUsers(registeredUsersList);
    
    setStyleName("dialog");
    
    this.setAutoHideEnabled(true);
    this.show();
    centerDialog();
  }

  //Metodo per centrare orizzontalmente la finestra
  private void centerDialog()
  {
    this.setPopupPosition(((Window.getClientWidth() - getOffsetWidth())/2), 100);
  }

  //Metodo che visualizza l'elenco degli utenti registrati, ma non giudici
  private void showUsers(ArrayList<User> registeredUsersList)
  {
    for(User u:registeredUsersList)
    {
      SingleUser user = new SingleUser(this, u.getUsername());
      registeredUsersListContainer.add(user);
    }
  }
  
  //Metodo che aggiorna l'elenco degli utenti registrati, ma non giudici
  public void refreshJudges(ArrayList<User> registered)
  {
    registeredUsersListContainer.clear();
    showUsers(registered);
  }
  
  //Metodo che richiede la nomina di un nuovo giudice
  public void nominateJudge(String username)
  {
    controller.nominateJudge(username);
  }
}
