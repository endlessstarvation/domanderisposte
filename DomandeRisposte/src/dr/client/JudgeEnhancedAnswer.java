package dr.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;

/**
 * Classe che gestisce la sezione dell'interfaccia grafica per
 * l'aggiunta della sezione di gestione delle risposte dell'utente giudice
 * alla sezione delle risposte della finestra di una domanda specifica
 */
public class JudgeEnhancedAnswer extends Composite
{
  interface JudgeEnhancedAnswerBinder extends UiBinder<Widget, JudgeEnhancedAnswer> {}
  private static JudgeEnhancedAnswerBinder uiBinder = GWT.create(JudgeEnhancedAnswerBinder.class);

  private SingleAnswer owner;
  
  @UiField ListBox judgement;
  @UiField Button judgeAnswer;
  @UiField Button deleteAnswer;
  
  public JudgeEnhancedAnswer(SingleAnswer owner)
  {
    this.owner = owner;
    
    initWidget(uiBinder.createAndBindUi(this));
    
    judgement.addItem(" ");
    for(int i=0; i<6; i++)
    {
      judgement.addItem(""+i);
    }
    
    judgement.getElement().setId("judgementListBox");
  }
  
  /*
   * Associo il clickHandler al Button attraverso l'UiHandler:
   * - judgeAnswer controlla che sia stato selezionato un giudizio e richiede il metodo per l'aggiunta di un nuovo giudizio alla risposta
   * - deleteAnswer richiede il metodo per la cancellazione di una risposta
   */
  @UiHandler(value={"judgeAnswer","deleteAnswer"})
  void handleClick(ClickEvent event)
  {
    Widget sender = (Widget) event.getSource();
    if(sender == judgeAnswer)
    {
      if(judgement.getSelectedIndex() != 0)
      {
        owner.addJudgement(judgement.getSelectedIndex()-1);
        judgement.setSelectedIndex(0);
      } else
      {
        owner.alertFailure("Selezionare un giudizio");
      }
    } else if(sender == deleteAnswer)
    {
      owner.deleteAnswer();
    }
  }
  
}
