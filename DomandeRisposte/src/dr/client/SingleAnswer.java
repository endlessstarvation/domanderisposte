package dr.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import dr.shared.Answer;

/**
 * Classe che gestisce la sezione dell'interfaccia grafica per
 */
public class SingleAnswer extends Composite
{
  interface SingleAnswerBinder extends UiBinder<Widget, SingleAnswer> {}
  private static SingleAnswerBinder uiBinder = GWT.create(SingleAnswerBinder.class);

  private QuestionDialog owner;
  private Answer answer;
  
  @UiField HorizontalPanel enhancedAnswerContainer;
  @UiField VerticalPanel verticalAnswerContainer;
  @UiField Label answerText;
  @UiField Label judgement;
  @UiField Label author;
  @UiField Label submissionDate;
  
  
  public SingleAnswer(QuestionDialog owner, Answer answer)
  {
    this.owner = owner;
    this.answer = answer;
    
    initWidget(uiBinder.createAndBindUi(this)); // SimpleAnswer instantiation through UiBinder
    
    answerText.setText(answer.getText());
    if(answer.getJudgements() != null)
    {
      judgement.setText(""+answer.getJudgementRate());
    } else
    {
      judgement.setText("0");
    }
    author.setText(answer.getAuthor());
    submissionDate.setText(answer.getFormattedDate());
    
    setStyleNames();
  }
  
  private void setStyleNames(){
	  enhancedAnswerContainer.setStyleName("answerContainer");
	  
	  judgement.getElement().setId("judgementValue");
	  answerText.getElement().setId("dialogTextElem");
	  submissionDate.getElement().setId("dateText");
	  author.getElement().setId("label");
  }
  
  public void addJudgeFunction()
  {
    verticalAnswerContainer.add(new JudgeEnhancedAnswer(this));
  }
  
  public void addAdminFunction()
  {
    verticalAnswerContainer.add(new AdminUIEnhancement(this));
  }
  
  //Metodo che richiama il metodo per inserire un nuovo giudizio
  public void addJudgement(int value)
  {
    owner.addJudgement(answer.getId(), value);
  }
  
  //Metodo che richiama il metodo per eliminare la specifica risposta
  public void deleteAnswer()
  {
    owner.deleteAnswer(answer.getId());
  }
  
  @UiHandler("judgement")
  void handleClick(ClickEvent event)
  {
    if(answer.getJudgements() != null)
    {
      new JudgementDialog(answer.getJudgements(), judgement.getAbsoluteLeft(), judgement.getAbsoluteTop());
    } else
    {
      alertFailure("La risposta non è ancora stata giudicata");
    }
  }

  //Metodo per creare una finestra di avvertimento
  public void alertFailure(String alertMessage)
  {
    owner.alertFailure(alertMessage);
  }
}
