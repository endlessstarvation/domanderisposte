package dr.client;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Classe che gestisce la sezione dell'interfaccia grafica dell'Amministatore per
 * la gestione delle categorie
 */
public class CategoryManagement extends DialogBox
{
  interface CategoryManagementBinder extends UiBinder<Widget, CategoryManagement> {}
  private static CategoryManagementBinder uiBinder = GWT.create(CategoryManagementBinder.class);
  
  private Controller controller;
  private List<String[]> categoryNameList;

  @UiField VerticalPanel categoryTree;
  @UiField VerticalPanel categoryManagement;
  
  public CategoryManagement(Controller controller, GUIHandler guiHandler,  List<String[]> categoryNameList)
  {
    this.controller = controller;
    /* Lista di coppie CategoryId-CategoryName che permette di ottenere l'id di una categoria
     * a partire dalla sua posizione all'interno della lista delle coppie*/
    this.categoryNameList = categoryNameList;
    
    setWidget(uiBinder.createAndBindUi(this));
    
    populateWindow();
    
    categoryTree.setStyleName("managerDiv");
    this.setStyleName("dialog");
    
    this.setAutoHideEnabled(true);
    this.center();
    this.show();
  }
  private void populateWindow()
  {
    showCategoryTree();
    addCategoryManager();
  }
  
  /*
   * Metodo che mostra l'elenco delle categorie nell'apposita sezione della finestra
   */
  private void showCategoryTree()
  {
    for(String[] s:categoryNameList)
    {
      categoryTree.add(new Label(s[1]));
    }
  }
  
  /*
   * Metodo che aggiorna l'elenco delle categorie nell'apposita sezione della finestra
   */
  public void refreshCategory(List<String[]> categoryNameList)
  {
    categoryTree.clear();
    categoryManagement.clear();
    this.categoryNameList = categoryNameList;
    showCategoryTree();
    addCategoryManager();
  }
  
  /*
   * Metodo che aggiunge le sezioni della finestra per:
   * - rinominare una categoria (CategoryRenamer)
   * - aggiungere una categoria (CategoryAdder)
   * - aggiungere una sottocategoria (SubCategoryAdder)
   */
  private void addCategoryManager()
  {
    categoryManagement.add(new CategoryRenamer(this, categoryNameList));
    categoryManagement.add(new CategoryAdder(this));
    categoryManagement.add(new SubCategoryAdder(this, categoryNameList));
  }
  
  /*
   * Metodo per inviare la richiesta di cambio del nome di una categoria
   */
  public void renameCategory(int categoryId, String newName)
  {
    controller.renameCategory(Integer.parseInt(categoryNameList.get(categoryId)[0]), newName);
  }
  
  /*
   * Metodo per inviare la richiesta di aggiunta di una categoria
   */
  public void addNewCategory(String newName)
  {
    controller.addCategory(newName);
  }
  
  /*
   * Metodo per inviare la richiesta di aggiunta di una sottocategoria
   */
  public void addNewSubCategory(int fatherIndex, String newName)
  {
    controller.addSubCategory(Integer.parseInt(categoryNameList.get(fatherIndex)[0]), newName);
  }
  
  public void alertFailure(String message)
  {
    controller.alertFailure(message);
  }
}
