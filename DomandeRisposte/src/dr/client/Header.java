package dr.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Classe che gestisce l'header dell'interfaccia utente
 */
public class Header extends SimplePanel
{
  interface HeaderBinder extends UiBinder<Widget, Header> {}
  private static HeaderBinder uiBinder = GWT.create(HeaderBinder.class);
  
  @UiField Button registration; //Button per la registrazione di un nuovo utente
  @UiField Button login; //Button per il login
  @UiField Button logout; //Button per il logout
  
  private GUIHandler owner;
  
  public Header(GUIHandler owner) 
  {
    //Associo il controller
    this.owner = owner;
    
    setWidget(uiBinder.createAndBindUi(this)); // Istanzio l'header attraverso l'UiBinder
    
    //Imposto l'ID dei Button per poterne gestire la posizione mediante la CSS
    login.getElement().setId("loginButton");
    logout.getElement().setId("logoutButton");
    registration.getElement().setId("registrationButton");
    
    //Inserisco i Button nel propio <div> di appartenenza
    RootPanel.get("LogButton").add(login);
    RootPanel.get("LogButton").add(logout);
    RootPanel.get("RegButton").add(registration);
    
    //Imposto i Button affinché sia mostrato il login/registrazione
    swapLogButtons(false);
  }
  
  //Associo il clickHandler ai Button attraverso l'UiHandler
  @UiHandler(value={"registration", "login", "logout"})
  void handleClick(ClickEvent event)
  {
    Widget sender = (Widget) event.getSource(); //Ottengo l'oggetto che ha generato l'evento di onClick
    if(sender == registration)
    {
      owner.createWidget("registration", null); //Richiamo il metodo per la generazione di una nuova finestra di Registrazione
    } else if(sender == login)
    {
      owner.createWidget("login", null); //Richiamo il metodo per la generazione di una nuova finestra di Login
    } else if(sender == logout)
    {
      owner.handleLogRequest(false);
    }
  }
  
  /*Metodo che gestisce la sostituzione dei bottoni di
   * Login: visualizzazione della finestra per effettuare il login
   * Logout: ripristina l'homepage alle condizioni iniziali
   * Registrati: visualizzazione della finestra per potersi registrare
   */
  public void swapLogButtons(boolean isLogin)
  {
    if(isLogin)
    {
      login.setVisible(false);
      registration.setVisible(false);
      logout.setVisible(true);
    } else
    {
      login.setVisible(true);
      registration.setVisible(true);
      logout.setVisible(false);
    }
  }
}