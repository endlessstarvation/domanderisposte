package dr.client;
import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.core.client.GWT;

import com.google.gwt.user.client.rpc.AsyncCallback;

import dr.shared.Answer;
import dr.shared.Category;
import dr.shared.DuplicatedUserNameException;
import dr.shared.Judgement;
import dr.shared.Question;
import dr.shared.QuestionExtended;
import dr.shared.User;
import dr.shared.UserNotFoundException;
import dr.shared.WrongPasswordException;

/**
 * Classe che gestisce la comunicazione con il server e 
 * le conseguenti richieste di modifica all'interfaccia grafica
 */
public class Controller {
	
  private DRServiceAsync service;
	private GUIHandler guiHandler;
	
	private User user;
	
	/*
	 * Costruttore della classe: quando invocato
	 * L'istanza del Server (DRService Async)
	 * L'istanza della classe adibita alla gestione dell'interfaccia utente (GUIHandler)
	 */
	public Controller()
	{
	  service = GWT.create(DRService.class);
	  guiHandler = new GUIHandler(this);
	}
	
	/* 
	 * Metodo per gestire l'avvio dell'applicazione:
	 * Creazione dell'header dell'applicazione
	 * Richiesta al server dell'elenco delle domande
	 */
	public void init()
	{
	  guiHandler.createHeader();
	  requestQuestions(0); 
	}
	
  //Metodo per creare una finestra di avvertimento
  public void alertFailure(String alertMessage)
  {
    guiHandler.createWidget("alert",alertMessage);
  }
	
	//Metodo per gestire l'evento di login e logout
	public void setLogged(User user)
  {
    if(user != null) //Evento di login
    {
      guiHandler.handleLogRequest(true); //Viene mostrato solo il bottone di logout
    	if(user.isAdmin()) //Creazione dell'interfaccia utente in funzione del ruolo dell'utente
    	{
    	  createAdminUI();
    	} else
    	{
    	  createRegisteredUI();
    	}
    } else //Evento di logout
    {
      requestQuestions(0);
    }
    this.user = user;
  }
	
	/*
	 * Metodo per gestire la richiesta di login:
	 * Se la richiesta va a buon fine, viene avvertito l'utente dell'avvenuto login e richiamato il metodo per gestire l'evento di login
	 * Se le richiesta non va a buon fine, viene avvertito l'utente del problema riscontrato
	 */
	public void login( String username, String password )
	{
	  //Richiesta di login al server 
	  service.login(username, password, new AsyncCallback<User>() //Invocazione del metodo di Login del server
    {
      @Override
      public void onFailure(Throwable caught)
      {
        try
        {
          throw caught; //Viene lanciata l'eccezione restituita dal server per poter evidenziare quale è stato il problema
        } catch (WrongPasswordException wp)
        {
          guiHandler.cleanFields("loging");
          alertFailure("Password inserita errata");
        } catch(UserNotFoundException e)
        {
          guiHandler.cleanFields("login");
          alertFailure("Utente non trovato nel database");
        }
        catch( Throwable e) //Problema generico
        {
          guiHandler.hideDialog();
          alertFailure("Non è stato possibile effettuare il login");
        }
      }
    
      @Override
      public void onSuccess(User returnedUser) 
      {
        guiHandler.hideDialog();
        guiHandler.createWidget("notify", "Login effettuato con successo"); //Viene avvertito l'utente che il login è stato effettuato con successo
        setLogged(returnedUser); //Viene aggiornata la GUI
      }
		});
	}
	
  /*
   * Metodo per gestire la richiesta di registrazione:
   * Se la richiesta va a buon fine, viene avvertito l'utente dell'avvenuta registrazione e richiamato il metodo per gestire l'evento di login
   * Se le richiesta non va a buon fine, viene avvertito l'utente del problema riscontrato
   */
  public void registration
    (
      String username, 
      String password, 
      String email, 
      String name, 
      String lastname, 
      String gender, 
      String birthdate, 
      String birthplace, 
      String address
    )
  {
    //Richiesta al server della registrazione del nuovo utente
    service.addUser(username, password, email, name, lastname, gender, birthdate, birthplace, address, new AsyncCallback<User>()
    {
      @Override
      public void onFailure(Throwable caught)
      {
        try
        { 
          throw caught; //Viene lanciata l'eccezione restituita dal server per poter evidenziare quale è stato il problema
        } catch ( DuplicatedUserNameException du )
        {
          guiHandler.cleanFields("registration");
          alertFailure("Attenzione l'username " + du.getUserName() + " non può essere scelto");
        } catch(Throwable e) //Problema generico
        {
          guiHandler.hideDialog(); 
          alertFailure("Non è stato possibile effettuare la registrazione");
        }
      }
    
      @Override
      public void onSuccess(User returnedUser)
      {
        guiHandler.hideDialog();
        guiHandler.createWidget("notify", "Registrazione effettuata con successo"); //Viene avvertito l'utente che la registazione è stata effettutata con successo
        setLogged(returnedUser); //Viene aggiornata la GUI
      }
    });
  }
  
  /*
   * Metodo per creare l'interfaccia grafica dell'utente registrato/giudice
   */
  private void createRegisteredUI()
  {
    AsyncCallback<Category> categoryRequestAsyncCallback = new AsyncCallback<Category>()
    {
  
      @Override
      public void onFailure(Throwable caught)
      {
        alertFailure("Non è stato possibile ottenere l'elenco delle categorie");
      }
      
      @Override
      public void onSuccess(Category result)
      {
        if(result != null)
        {
          guiHandler.createWidget("categoryBoard",result); //Creazione della sezione per la selezione delle domande per categoria
          guiHandler.createWidget("question", result); //Creazione della sezione per l'inserimento di una nuova domanda
        } else 
        {
          alertFailure("Nessuna categoria disponibile");
        }
      }
    };
    service.getCategoryTree(categoryRequestAsyncCallback); //Richiesta al server dell'albero delle categorie
  }
  
  /*
   * Metodo per richiedere la lista delle domande:
   * Se la richiesta va a buon fine viene mostrato l'elenco delle domande restituito dal server
   * Se la richiesta non va a buon fine viene avvertito l'utente del problema
   */
  public void requestQuestions(int id)
  {
    AsyncCallback<ArrayList<Question>> questionRequestAsyncCallback = new AsyncCallback<ArrayList<Question>>()
    {
      @Override
      public void onFailure(Throwable caught)
      {
        alertFailure("Non è stato possibile ottenere l'elenco delle domande");
      }
      
      @Override
      public void onSuccess(ArrayList<Question> questions)
      {
        showQuestions(questions);
      }
    };
    
    if(id == 0) //La categoria generica ha id = 0
    {
      service.getAllQuestions(questionRequestAsyncCallback); //Richiesta al server dell'elenco di tutte le domande
    } else //Ogni altra categoria ha id > 0
    {
      service.getAllQuestionsByCategory(id, questionRequestAsyncCallback); //Richiesta al server dell'elenco di tutte le domande appartenenti a una categoria
    }
  }
  
  /*
   * Metodo per mostrare l'elenco delle domande nella colonna centrale
   * della pagina in funzione del ruolo dell'utente
   */
  private void showQuestions(ArrayList<Question> questions)
  {
    guiHandler.clearQuestionColumn();
    if(user != null && user.isAdmin())
    {
      guiHandler.showAdminQuestions(questions);
    }
    else
    {
      guiHandler.showQuestions(questions);
    }
  }
  
  /*
   * Metodo per richiedere l'inserimento di una nuova domanda:
   * Se la richiesta va a buon fine viene aggiornato l'elenco delle domande restituito dal server
   * Se la richiesta non va a buon fine viene avvertito l'utente del problema
   */
  public void addNewQuestion(String text, int categoryId)
  {
    AsyncCallback<ArrayList<Question>> addNewQuestion = new AsyncCallback<ArrayList<Question>>()
    {
      @Override
      public void onFailure(Throwable caught)
      {
        
        alertFailure("Non è stato possibile inserire la domanda");
      }
      
      @Override
      public void onSuccess(ArrayList<Question> questions)
      {
        showQuestions(questions);
      }
    };
    service.addQuestion(text, user.getUsername(), categoryId, addNewQuestion); //Richiesta al server di aggiunta di una nuova domanda
  }
  
  /*
   * Metodo che richiede al server una domanda specifica:
   * Se la richiesta va a buon fine viene mostrata la finestra contenente la domanda specifica
   * Se la richiesta non va a buon fine viene avvertito l'utente del problema
   */
  public void extendedQuestionDialog(int questionId)
  {
    AsyncCallback<QuestionExtended> requestFullQuestion = new AsyncCallback<QuestionExtended>()
    {
      @Override
      public void onFailure(Throwable caught)
      {
        alertFailure("Non è stato possibile ottenere la domanda");
      }
      
      @Override
      public void onSuccess(QuestionExtended question)
      {
        showQuestionDialog(question);
      }
    };
    service.getFullQuestion(questionId, requestFullQuestion); //Richiesta al server di una domanda specifica
  }
  
  //Metodo che crea la finestra di una specifica domanda
  private void showQuestionDialog(QuestionExtended question)
  {
    if(user != null)
    {
      guiHandler.showQuestionDialog(question, user.getRole());
    } else
    {
      guiHandler.showQuestionDialog(question, 'v');
    } 
  }
  
  /*
   * Metodo per richiedere l'inserimento di una nuova risposta
   * Se la richiesta va a buon fine viene aggiornato l'elenco delle risposte nella finestra della domanda specifica
   * Se la richiesta non va a buon fine viene avvertito l'utente del problema
   */
  public void addNewAnswer(String text, int id)
  {
    AsyncCallback<QuestionExtended> addNewAnswerCallback = new AsyncCallback<QuestionExtended>()
    {
      @Override
      public void onFailure(Throwable caught)
      {
        guiHandler.cleanFields("answer");
        alertFailure("Non è stato possibile inserire la risposta");
      }
      
      @Override
      public void onSuccess(QuestionExtended question)
      {
        guiHandler.cleanFields("answer");
        guiHandler.updateQuestionDialog(question, user.getRole());
      }
    };
    service.addAnswer(text, id, user.getUsername(), new Date(), addNewAnswerCallback); //Richiesta al server di aggiunta della risposta
  }
  
  /*
   * Metodo per aggiungere un nuovo giudizio a una risposta:
   * Se la richiesta va a buon fine viene aggiornato il giudizio medio della risposta nella finestra della domanda specifica
   * Se la richiesta non va a buon fine viene avvertito l'utente del problema
   */
  public void addJudgement(QuestionExtended q, int answerId, int value)
  {
    AsyncCallback<QuestionExtended> addNewJudgementCallback = new AsyncCallback<QuestionExtended>()
    {
      @Override
      public void onFailure(Throwable caught)
      {
        alertFailure("Non è stato possibile aggiungere il giudizio");
      }
      
      @Override
      public void onSuccess(QuestionExtended question)
      {
        guiHandler.updateQuestionDialog(question, user.getRole());
      }
    };
    //Contollo che il giudice non stia giudicando una propria risposta o stia esprimendo un duplice giudizio
    if(checkNotAnswerAuthor(q.getAnswerById(answerId)))
    {
      if(checkNotAlreadyJudged(q.getAnswerById(answerId)))
      {
        //Richiesta al server di aggiunta del giudizio alla risposta
        service.addJudgement(q.getId(), answerId, user.getUsername(), value, addNewJudgementCallback);
      } else
      {
        alertFailure("Non è consentito esprimere molteplici giudizi sulla stessa risposta");
      }
    } else
    {
      alertFailure("Non è consentito esprimere un giudizio su una propria risposta");
    }
  }
  
  //Metodo che controlla che il giudice non stia giudicando una propria risposta
  private boolean checkNotAnswerAuthor(Answer a)
  {
    return !a.getAuthor().equals(user.getUsername());
  }
  
  //Metodo che controlla che il giudice non abbia già giudicato la risposta
  private boolean checkNotAlreadyJudged(Answer a)
  {
    boolean notJudged = true;
    if(a.getJudgements() != null)
    {
      for(Judgement j:a.getJudgements())
      {
        if(j.getAuthor().equals(user.getUsername()))
        {
          notJudged = false;
        }
      }
    }
    return notJudged;
  }
  
  /*
   * Metodo che richiede l'eliminazione di una risposta:
   * Se la richiesta va a buon fine viene aggiornato l'elenco delle risposte nella finestra della domanda specifica
   * Se la richiesta non va a buon fine viene avvertito l'utente del problema
   */
  public void deleteAnswer(int id, int answerId)
  {
    AsyncCallback<QuestionExtended> deleteAnswerCallback = new AsyncCallback<QuestionExtended>()
    {
      @Override
      public void onFailure(Throwable caught)
      {
        alertFailure("Non è stato possibile eliminare la risposta");
      }
      
      @Override
      public void onSuccess(QuestionExtended question)
      {
        guiHandler.updateQuestionDialog(question, user.getRole());
      }
    };
    service.removeAnswer(id, answerId, deleteAnswerCallback); //Richiesta al server di rimozione di una risposta
  }
  
  /*
   * Metodo per richiedere l'eliminazione di una domanda
   * Se la richiesta va a buon fine viene aggiornato l'elenco delle domande
   * Se la richiesta non va a buon fine viene avvertito l'utente del problema
   */
  public void deleteQuestion(int id)
  {
    AsyncCallback<ArrayList<Question>> deleteQuestionCallback = new AsyncCallback<ArrayList<Question>>()
    {
      @Override
      public void onFailure(Throwable caught)
      {
        alertFailure("Non è stato possibile eliminare la domanda");
      }
      
      @Override
      public void onSuccess(ArrayList<Question> questions)
      {
        showQuestions(questions);
      }
    };
    service.removeQuestion(id, deleteQuestionCallback); //Richiesta al server di rimozione di una domanda
  }
  
  //Metodo che genera l'interfaccia utente dell'Amministratore
  private void createAdminUI()
  {
    //Creazione dell'elenco delle domande per l'Amministratore
    requestQuestions(0);
    //Creazione pannello per la gestione delle categorie e per la gestione del ruolo degli utenti
    guiHandler.createWidget("admin", null);
  }
  
  /*
   * Metodo per richiedere l'elenco delle categorie:
   * Se la richiesta va a buon fine:
   *  se l'elenco delle categorie non è vuoto viene mostrata la finestra di gestione delle categorie
   *  se l'elenco delle categorie è vuoto viene avvertito l'Amministratore
   * Se la richiesta non va a buon fine viene avvertito l'Amministratore del problema
   */
  public void getManagementCategory()
  {
    AsyncCallback<Category> categoryRequestAsyncCallback = new AsyncCallback<Category>()
    {

      @Override
      public void onFailure(Throwable caught)
      {
        alertFailure("Non è stato possibile ottenere l'elenco delle categorie");
      }
      
      @Override
      public void onSuccess(Category result)
      {
        if(result != null)
        {
          guiHandler.createWidget("categoryManagement", result);
        } else 
        {
          alertFailure("Nessuna categoria disponibile.");
        }
      }
     };
     
    service.getCategoryTree(categoryRequestAsyncCallback); //Richiesta al server dell'albero delle categorie
  }
  
  /*
   * Metodo per rinominare una categoria:
   * Se la richiesta va a buon fine viene aggiornato l'elenco delle categorie nella finestra di gestione delle categorie
   * Se la richiesta non va a buon fine viene avvertito l'Amministratore del problema
   */
  public void renameCategory(int categoryId, String newName)
  {
    AsyncCallback<Category> renameCategoryCallback = new AsyncCallback<Category>()
    {
      @Override
      public void onFailure(Throwable caught)
      {
        alertFailure("Non è stato possibile rinominare la categoria");
      }
      
      @Override
      public void onSuccess(Category category)
      {
        guiHandler.updateCategoryList(category);
      }
    };
    service.renameCategory(categoryId, newName, renameCategoryCallback); //Richiesta al server di rinominare la categoria
  }
  
  /*
   * Metodo per aggiungere una categoria:
   * Se la richiesta va a buon fine viene aggiornato l'elenco delle categorie nella finestra di gestione delle categorie
   * Se la richiesta non va a buon fine viene avvertito l'Amministratore del problema
   */
  public void addCategory(String name)
  {
    AsyncCallback<Category> addCategoryCallback = new AsyncCallback<Category>()
    {
      @Override
      public void onFailure(Throwable caught)
      {
        alertFailure("Non è stato possibile aggiungere la categoria");
      }
      
      @Override
      public void onSuccess(Category category)
      {
        guiHandler.updateCategoryList(category);
      }
    };
    service.addCategory(name, addCategoryCallback);
  }
  
  /*
   * Metodo per aggiungere una sottocategoria a una categoria esistente
   * Se la richiesta va a buon fine viene aggiornato l'elenco delle categorie nella finestra di gestione delle categorie
   * Se la richiesta non va a buon fine viene avvertito l'Amministratore del problema
   */  
  public void addSubCategory(int fatherId, String name)
  {
    AsyncCallback<Category> addCategoryCallback = new AsyncCallback<Category>()
    {
      @Override
      public void onFailure(Throwable caught)
      {
        alertFailure("Non è stato possibile aggiungere la sotto categoria");
      }
      
      @Override
      public void onSuccess(Category category)
      {
        guiHandler.updateCategoryList(category);
      }
    };
    service.addSubCategory(fatherId, name, addCategoryCallback);
  }
  
  /*
   * Metodo per richiedere l'elenco degli utenti registrati ma non giudici
   * Se la richiesta va a buon fine viene creata la finestra di gestione delle promozioni
   * Se la richiesta non va a buon fine viene avvertito l'Amministratore del problema
   */
  public void getRegisteredUsersBoard()
  {
    AsyncCallback<ArrayList<User>> getRegisteredUsersCallback = new AsyncCallback<ArrayList<User>>()
    {
      @Override
      public void onFailure(Throwable caught)
      {
        alertFailure("Non è stato possibile ottenere l'elenco degli utenti registrati");
      }
      
      @Override
      public void onSuccess(ArrayList<User> registeredUsers)
      {
        if(registeredUsers.size() != 0)
        {
          guiHandler.createWidget("judges", registeredUsers);;
        } else
        {
          alertFailure("Attenzione tutti gli utenti registrati sono dei giudici");
        }
      }
    };
    service.getRegistered(getRegisteredUsersCallback); //Richiesta al server dell'elenco degli utenti registrati, ma non giudici
  }
  
  /*
   * Metodo per nominare un nuovo giudice:
   *Se la richiesta va a buon fine viene aggiornato l'elenco degli utenti registrati, ma non giudici
   * Se la richiesta non va a buon fine viene avvertito l'Amministratore del problema
   */
  public void nominateJudge(String username)
  {
    final String user = username;
    AsyncCallback<ArrayList<User>> nominateJudgeCallback = new AsyncCallback<ArrayList<User>>()
    {
      @Override
      public void onFailure(Throwable caught)
      {
        alertFailure("Non è stato possibile rendere "+user+" un nuovo giudice");
      }
      
      @Override
      public void onSuccess(ArrayList<User> registered)
      {
        if(registered.size() != 0)
        {
          guiHandler.updateJudges(registered);
        } else
        {
          guiHandler.hideDialog();
          alertFailure("Attenzione tutti gli utenti registrati sono dei giudici");
        }
      }
    };
    service.nominateJudge(username, nominateJudgeCallback); //Richiesta al server di nomina del giudice
  }
}
