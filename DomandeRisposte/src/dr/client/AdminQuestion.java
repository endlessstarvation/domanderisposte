package dr.client;


import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

import dr.shared.Question;

/**
 * Classe che gestisce la sezione dell'interfaccia grafica dell'Amministratore per
 * la visualizzazione della domanda con il Button per l'eliminazione
 */
public class AdminQuestion extends Composite
{
  interface AdminQuestionBinder extends UiBinder<Widget, AdminQuestion> {}
  private static AdminQuestionBinder uiBinder = GWT.create(AdminQuestionBinder.class);

  @UiField HTMLPanel questionContainer;
  
  private Controller controller;
  private Question question;
  
  public AdminQuestion(Controller controller, Question q) 
  {
    this.controller = controller;
    this.question = q; //Associo la specifica domanda al widget che ne consente la visualizzazione
    
    initWidget(uiBinder.createAndBindUi(this));
    
    questionContainer.setStyleName("adminQuestionContainer");
    questionContainer.add(new QuestionButton(controller, q));
    questionContainer.add(new AdminUIEnhancement(this));
  }
  
  //Richiamo il metodo per la cancellazione della domanda
  public void deleteQuestion()
  {
    controller.deleteQuestion(question.getId());
  }
}
