package dr.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

/**
 * Classe che gestisce i Button dell'interfaccia grafica dell'Amministratore per
 * la cancellazione di una domanda o di una risposta
 */
public class AdminUIEnhancement extends Composite
{
  interface AdminUIEnhancementBinder extends UiBinder<Widget, AdminUIEnhancement> {}
  private static AdminUIEnhancementBinder uiBinder = GWT.create(AdminUIEnhancementBinder.class);

  private Composite owner;
  
  @UiField Button delete;
  
  public AdminUIEnhancement(Composite owner)
  {
    this.owner = owner;
    
    initWidget(uiBinder.createAndBindUi(this));
    
    delete.setStyleName("adminDeleteButton");
  }
  
  /*
   * Associo il clickHandler al Button attraverso l'UiHandler
   * Se la richiesta di cancellazione viene da una risposta allora richiamo il medodo di cancellazione di una risposta
   * Se la richiesta di cancellazione viene da una domanda allora richiamo il medodo di cancellazione di una domanda
   */
  @UiHandler("delete")
  void handleClick(ClickEvent event)
  {
    if(owner instanceof SingleAnswer)
      ((SingleAnswer) owner).deleteAnswer();
    else
      ((AdminQuestion)owner).deleteQuestion();
  }
  
}
