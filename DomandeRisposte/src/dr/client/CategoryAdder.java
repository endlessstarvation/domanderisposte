package dr.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

/**
 * Classe che gestisce la sezione dell'interfaccia grafica dell'Amministatore per
 * l'aggiunta di una nuova categoria
 */
public class CategoryAdder extends Composite
{
  interface CategoryAdderBinder extends UiBinder<Widget, CategoryAdder> {}
  private static CategoryAdderBinder uiBinder = GWT.create(CategoryAdderBinder.class);
  
  private CategoryManagement owner;
  
  @UiField TextBox newName;
  @UiField Button add;
  @UiField Label title;
  
  public CategoryAdder(CategoryManagement owner)
  {
    this.owner = owner;
    
    initWidget(uiBinder.createAndBindUi(this));
    
    title.getElement().setId("label");
    newName.setStyleName("managerItem");
    add.setStyleName("managerItem");
    this.setStyleName("managerDiv");

  }
  
  /*
   * Associo il clickHandler al Button attraverso l'UiHandler
   * Se è stato specificato il nome della nuova categoria allora viene effettuata la richiesta d'inserimento di una nuova categoria
   * Se non è stato specificato il nome della nuova categoria allova viene avvertito l'utente
   */
  @UiHandler("add")
  void handleClick(ClickEvent event) {
    if(!newName.getText().isEmpty())
    {
      owner.addNewCategory(newName.getText());
    } else
    {
      owner.alertFailure("Inserire il nome della nuova categoria");
    }
  }
}
