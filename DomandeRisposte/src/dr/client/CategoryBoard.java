package dr.client;

import java.util.ArrayList;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.VerticalPanel;

import dr.shared.Category;

public class CategoryBoard extends Composite
{

  @UiField Tree staticTree;

  private Controller controller;

  public CategoryBoard(Controller controller, Category categoryTree)
  {
    this.controller = controller;
    drawCategoryTree(categoryTree);
  }

  private void drawCategoryTree(Category categoryTree)
  {
    staticTree = createStaticTree(categoryTree);
    staticTree.setAnimationEnabled(true);

    // Wrap the static tree in a scrollPanel
    //ScrollPanel staticTreeWrapper = new ScrollPanel(staticTree);
    //staticTreeWrapper.setSize("300px", "300px");

    // Wrap the static tree in a VerticalPanel
    VerticalPanel panel = new VerticalPanel();
    panel.add(staticTree);

    // Adding onClick event
    addHandler();
    
    RootPanel.get("Categories").add(panel);
  }

  private void addHandler()
  {
	  staticTree.addSelectionHandler(new SelectionHandler<TreeItem>(){
		@Override
		public void onSelection(SelectionEvent<TreeItem> event) {
			CategoryItem selected = (CategoryItem) event.getSelectedItem();
			controller.requestQuestions(selected.getId());
		}
	  });
  }


  private Tree createStaticTree(Category root)
  {
    Tree staticTree = new Tree();

    // Costruisco la struttura di Category corrispondente
    CategoryItem item_root = visit(root.getSubcategories(), null);
    staticTree.addItem(item_root);
    // Return the tree
    return staticTree;
  }

  private CategoryItem visit(ArrayList<Category> categories, CategoryItem father)
  {
    if(father == null) // Se è la radice
    {
      father = new CategoryItem(null);
    }
    if(categories != null) //Se la categoria corrente ha sottocategorie
    {
      for(int i = 0; i < categories.size(); i++)
      {
        CategoryItem child = new CategoryItem(categories.get(i));
        father.addItem(child);
        // Ricorro nelle sottocategorie
        visit(categories.get(i).getSubcategories(), child);
      }
    }
    return father;
  }
}
