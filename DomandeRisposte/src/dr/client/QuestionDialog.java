package dr.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import dr.shared.Answer;
import dr.shared.QuestionExtended;

/**
 * Classe che gestisce la sezione dell'interfaccia grafica per
 * la visualizzazione di una domanda specifica
 */
public class QuestionDialog extends DialogBox
{
  interface QuestionDialogBinder extends UiBinder<Widget, QuestionDialog> {}
  private static QuestionDialogBinder uiBinder = GWT.create(QuestionDialogBinder.class);
  
  @UiField Label category;
  @UiField Label date;
  @UiField Label questionText;
  @UiField VerticalPanel answers;
  @UiField HTMLPanel questionDialog;
  
  private Controller controller;
  private QuestionExtended question;
  private NewAnswer newAnswer;
  
  public QuestionDialog(Controller controller, QuestionExtended question) 
  {
    this.controller = controller;
    this.question = question;
    
    setWidget(uiBinder.createAndBindUi(this)); 
    
    category.setText(question.getCategoryPath());
    date.setText(question.getFormattedDate());
    questionText.setText(question.getText());

    setStyleNames();
    
    this.setAutoHideEnabled(true);
    this.show();
  }
  
  private void setStyleNames(){
	  setStyleName("dialog");
	  
	  category.getElement().setId("label");
	  date.getElement().setId("dateText");
	  questionText.getElement().setId("dialogTextElem");
  }

  //Metodo per centrare orizzontalmente la finestra
  public void centerDialog()
  {
    this.setPopupPosition(((Window.getClientWidth() - getOffsetWidth())/2), 100);
  }

  //Metodo che visualizza lo spazio per l'inserimento di una nuova risposta
  public void showNewAnswer()
  {
    newAnswer = new NewAnswer(this);
    questionDialog.add(newAnswer);
  }
  
  //Metodo che visualizza le risposte associate alla domanda
  public void showAnswers()
  {
    if(question.getAnswers()!=null)
    {
      for(Answer a:question.getOrderedAnswers())
      {
        SingleAnswer answer = new SingleAnswer(this, a);
        answers.add(answer);
      }
    }
  }
  
  //Metodo che visualizza le risposte associate alla domanda
  public void showJudgeAnswers()
  {
    if(question.getAnswers()!=null)
    {
      for(Answer a:question.getOrderedAnswers())
      {
        SingleAnswer answer = new SingleAnswer(this, a);
        answer.addJudgeFunction();
        answers.add(answer);
      }
    }
  }
  
  //Metodo che visualizza le risposte associate alla domanda
  public void showAdminAnswers()
  {
    if(question.getAnswers()!=null)
    {
      for(Answer a:question.getOrderedAnswers())
      {
        SingleAnswer answer = new SingleAnswer(this, a);
        answer.addAdminFunction();
        answers.add(answer);
      }
    }
  }
  
  //Metodo per l'aggiornamento della domanda di riferimento dell'oggetto
  public void updateDialog(QuestionExtended question)
  {
    this.question = question;
    answers.clear();
  }
  
  //Metodo che richiama il metodo per inserire una nuova risposta
  public void addAnswer(String text)
  {
    controller.addNewAnswer(text, question.getId());
  }
  
  //Metodo che richiama il metodo per inserire un nuovo giudizio
  public void addJudgement(int answerId, int value)
  {
    controller.addJudgement(question, answerId, value);
  }
  
  //Metodo che richiama il metodo per cancellare una domanda
  public void deleteAnswer(int answerId)
  {
    controller.deleteAnswer(question.getId(), answerId);
  }
  
  //Metodo per ripulire la TextArea per l'inserimento di una nuova risposta
  public void clearTextArea()
  {
   newAnswer.clearTextArea(); 
  }
  
  //Metodo per creare una finestra di avvertimento
  public void alertFailure(String alertMessage)
  {
    controller.alertFailure(alertMessage);
  }
}
