package dr.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

/**
 * Classe che gestisce la sezione dell'interfaccia grafica dell'Amministratore per
 * la visualizzazione del singolo utente registrato, ma non giudice, nella finestra
 * di gestione delle promozioni
 */
public class SingleUser extends Composite
{
  interface SingleJudgementBinder extends UiBinder<Widget, SingleUser> {}
  private static SingleJudgementBinder uiBinder = GWT.create(SingleJudgementBinder.class);
  
  private JudgeNominationBoard owner;
  
  @UiField Label username;
  @UiField Button promote;
  @UiField HorizontalPanel userContainer;
  
  public SingleUser(JudgeNominationBoard owner, String username)
  {
    this.owner = owner;
    
    initWidget(uiBinder.createAndBindUi(this));
    
    this.username.setText(username);
    
    userContainer.setStyleName("managerDiv");
    promote.getElement().setId("promotionButton");
    this.username.getElement().setId("label");
  }
  
  //Associo il clickHandler al Button attraverso l'UiHandler
  @UiHandler("promote")
  void handleClick(ClickEvent event)
  {
    owner.nominateJudge(username.getText()); //Richiesta di promozione dell'utente
  }
}
