package dr.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

/**
 * Classe che gestisce la finestra di dialogo per notifiche all'utente
 */
public class Dialog extends DialogBox
{
  interface DialogBinder extends UiBinder<Widget, Dialog> {}
  private static DialogBinder uiBinder = GWT.create(DialogBinder.class);
  
  @UiField Button ok;
  @UiField Label message;
  
  public Dialog(String type, String message)
  {
    setWidget(uiBinder.createAndBindUi(this));
    if(type.equals("alert"))
    {
      this.setText("Avviso");
    } else if(type.equals("notify"))
    {
      this.setText("Notifica");
    }
    this.message.setText(message);
    setAutoHideEnabled(true);
    this.center();
    this.show();
  }

  //Associo il clickHandler al Button attraverso l'UiHandler
  @UiHandler("ok")
  void handleClick(ClickEvent event)
  {
    this.hide();
  }
}
