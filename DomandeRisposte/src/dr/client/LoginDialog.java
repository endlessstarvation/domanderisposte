package dr.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

/**
 * Classe che gestisce la sezione dell'interfaccia grafica per
 */
public class LoginDialog extends DialogBox
{
  interface LoginBinder extends UiBinder<Widget, LoginDialog> {}
  private static LoginBinder uiBinder = GWT.create(LoginBinder.class);
  
  @UiField TextBox username;
  @UiField PasswordTextBox password;
  @UiField Button login;
  @UiField Button cancel;
  
  private Controller controller;
  
  public LoginDialog(Controller controller) 
  {
    this.controller = controller;
    
    setWidget(uiBinder.createAndBindUi(this));
    
    // Impostazione del placeholder dei vari Textfield
    username.getElement().setPropertyString("placeholder", "Inserisci il tuo username");
    password.getElement().setPropertyString("placeholder", "Inserisci la tua password");
    
    this.getElement().setId("LoginDialog");
    
    this.setText("Login");
    this.center();
    this.show();
  }
  
  //Associo il clickHandler ai Button attraverso l'UiHandler
  @UiHandler(value={"login", "cancel"})
  void handleClick(ClickEvent event) {
    Widget sender = (Widget) event.getSource();
    if(sender == login)
    {
      login();
    } else if(sender == cancel)
    {
      this.hide();
    }
  }
  
  @UiHandler({"username","password"})
  void onEnterKeyPress(KeyPressEvent event) 
  {
    if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_ENTER)
    {
      login();
    }
  }

  /*
   * Metodo che
   *  se tutti i TextField sono stati riempiti, effettua una richiesta di login
   *  altriment ripulisce tutti i TextField e avverte l'utente
   */
  private void login()
  {
    if(!isEmpty())
    {
      controller.login(username.getText(), password.getText());
    } else
    {
      cleanFields();
      controller.alertFailure("Attenzione riempire tutti i campi");
      setUsernameFocus();
    }
  }
  
  /*
   * Metodo che controlla il corretto riempimento di tutti i campi
   */
  private boolean isEmpty()
  {
    if(username.getText().isEmpty() || password.getText().isEmpty())
    {
      return true;
    }
    return false;
  }
  
  //Metodo che ripulisce i TextField della finestra di login
  public void cleanFields()
  {
    username.setText("");
    password.setText("");
  }
  
  //Metodo che richiede il focus sul TextField username quando viene mostrata la DialogBox
  @Override
  public void show()
  {
    super.show();

    setUsernameFocus();
  }
  
  //Metodo che setta il focus sul TextField username
  public void setUsernameFocus()
  {
    Scheduler.get().scheduleDeferred(new Command()
    {
      public void execute()
      {
        username.setFocus(true);
      }
    });
  }
}
