package dr.shared;
/*
 *  L'oggetto Question rappresenta una versione minimizzata della Domanda contenente 
 *  soltanto l'id e il testo.
 */

import java.io.Serializable;
public class Question implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String text;
	private int id;
	
	public Question(){}
	public Question(int id, String text){
		this.id = id;
		this.text = text;
	}
	
	//GET Statements
	public String getText(){
		return text;
	}
	public int getId(){
		return id;
	}
}
