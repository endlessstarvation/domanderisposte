package dr.shared;
/*
 * Eccezione lanciata in caso di Password errata
 */
public class WrongPasswordException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
}
