package dr.shared;
/*
 * L'oggetto User rappresenta un qualsiasi tipo di utente.
 * La variabile privata @role identifica il ruolo dell'utente all'interno dell'applicazione.
 * Inoltre contiene tutti i valori opzionali previsti.
 */
import java.io.Serializable;

public class User implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String username, password, email;
	
	// 'r' = registered
	// 'a' = admin
	// 'j' = judge
	private char role;
	
	//Optional data
	private String name;
	private String surname;
	private String sex;
	private String birthDate;
	private String birthPlace;
	private String address;
	
	// Empty constructor, for GWT compiler.
	@SuppressWarnings ( "unused")
	private User(){}
	public User(String username, String password, String email, String name, String surname, String sex, String birthDate, String birthPlace, String address){
		this.username = username;
		this.password = password;
		this.email = email;
		
		//Optional data
		this.name = name;
		this.surname = surname;
		this.sex = sex;
		this.birthDate = birthDate;
		this.birthPlace = birthPlace;
		this.address = address;
	}
	
	//GET Statements
	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public String getSex() {
		return sex;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public String getBirthPlace() {
		return birthPlace;
	}

	public String getAddress() {
		return address;
	}

	public String getUsername(){
		return username;
	}
	
	public String getPassword(){
		return password;
	}
	
	public String getEmail(){
		return email;
	}
	public boolean isAdmin(){
		return this.role == 'a';
	}
	public boolean isJudge(){
		return this.role == 'j';
	}
	
	public boolean isRegistered(){
		return this.role == 'r';
	}
	public char getRole()
	{
	  return role;
	}
	
	
	public void setRoleAdmin(){
		this.role = 'a';
	}
	public void setRoleRegistered(){
		this.role = 'r';
	}
	public void setRoleJudge(){
		this.role = 'j';
	}
}
