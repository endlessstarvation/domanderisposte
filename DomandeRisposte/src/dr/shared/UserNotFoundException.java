package dr.shared;
/*
 * Eccezione lanciata in caso di Utente non trovato
 */
public class UserNotFoundException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
}
