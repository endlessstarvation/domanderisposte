package dr.shared;
/*
 * Eccezione lanciata in caso di Username dupplicato
 */
public class DuplicatedUserNameException extends Exception{
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String userName;
    
    @SuppressWarnings ( "unused")
	private DuplicatedUserNameException(){
    	
    }
    public DuplicatedUserNameException( String userName){
    	this.userName = userName;
    }
    
    public String getUserName(){
    	return this.userName;
    }
}
