package dr.shared;
/*
 *  L'oggetto Question rappresenta una estesa della Domanda contenente 
 *  inoltre il category_path di appartenenza, la propria lista di risposte con i relativi giudizi.
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import com.google.gwt.i18n.client.DateTimeFormat;

public class QuestionExtended extends Question implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String author;
	private String categoryPath;
	private ArrayList<Answer> answers;
	private Date submit_date;
	private int categoryId;

	public QuestionExtended(){}
	
	public QuestionExtended( int id, String text, String author, String categoryPath, Date submit_date, int categoryId ){
		super( id,text );
		this.author = author;
		this.categoryPath = categoryPath;
		this.submit_date = submit_date;
		this.categoryId = categoryId;
	}
	
	//GET Statements
	public String getCategoryPath() {
		return categoryPath;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public ArrayList<Answer> getAnswers(){
		return answers;
	}
	public Date getDate(){
		return submit_date;
	}
	public String getFormattedDate(){
		DateTimeFormat fmt = DateTimeFormat.getFormat("dd MMMM, HH:mm:ss");
		return fmt.format(submit_date);
	}
	public String getAuthor() {
		return author;
	}
	public Answer getAnswerById( int answerId ){
		for(int  i = 0; i < answers.size(); i++ ){
			if( answers.get( i ).getId() == answerId ){
				return answers.get( i );
			}
		}
		return null;
	}

	/*
	 * Algoritmo di ordinamento delle risposte.
	 * l'oggetto Comparator confronta due ipotetiche answer prima per
	 * JudgementRate e se questo coincide (comparing == 0), confronta le date.
	 * Ritorna l'array di risposte ordinato come da specifica.
	 */
	public ArrayList<Answer> getOrderedAnswers(){
		Collections.sort(answers, new Comparator<Answer>(){
			@Override
			public int compare(Answer o1, Answer o2) {
				Integer val = o2.getJudgementRate();
				
				int comparing = val.compareTo(o1.getJudgementRate());
				
				if(comparing==0){
					Date d1 = o1.getDate();
					Date d2 = o2.getDate();
					if(d2.after(d1)){
						comparing = 1;
					} else {
						comparing = -1;
					}
				} 
				return comparing;
			}
		});	
		return answers;
	}
	
	
	public void setCategoryPath(String categoryPath) {
		this.categoryPath = categoryPath;
	}
	
	public void setTimestamp(){
		this.submit_date = new Date();
	}
	
	public void addAnswer( Answer ans ){
		if(answers == null){
			answers = new ArrayList<Answer>();
		}
		answers.add( ans );
	}
	
	public void removeAnswer( int answerId ){
		for(int  i = 0; i < answers.size(); i++ ){
			if( answers.get( i ).getId() == answerId ){
				answers.remove( i );
				break;
			}
		}
	}
	
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

}
