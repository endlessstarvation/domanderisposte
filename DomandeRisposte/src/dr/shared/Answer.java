package dr.shared;

/*
 * L'oggetto Answer rappresenta una risposta appartenente ad una domanda
 * Contiene i dati per risalire all'autore e una lista di di Giudizi (judgements)
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import com.google.gwt.i18n.client.DateTimeFormat;

public class Answer implements Serializable{

	private static final long serialVersionUID = 1L;
  	private String text;
  	private int id;
  	private Date date;
  	private String author;
  	private ArrayList<Judgement> judgements;
	
	public Answer(){}
	public Answer( int id, String text, String author, Date date ){
		this.date = date;
		this.text = text;
		this.author = author;
		this.id = id;
	}
	
	// GET Statements
	public String getText(){
		return text;
	}
	public int getId() {
		return id;
	}
	public String getAuthor() {
		return author;
	}
	public Date getDate(){
		return date;
	}
	public ArrayList<Judgement> getJudgements(){
		return judgements;
	}
	// Metodo che ritorna la data formattata per la visualizzazione
	public String getFormattedDate(){
		DateTimeFormat fmt = DateTimeFormat.getFormat("dd MMMM, HH:mm:ss");
		return fmt.format(date);
	}
	//Metodo che calcola e ritorna la media dei giudizi in intero
	public int getJudgementRate(){
		int avg = 0;
		if(judgements != null){
			for( int i=0; i<judgements.size(); i++){
				avg = avg + judgements.get(i).getVal();
			}
			avg = avg/judgements.size();
		}
		return avg;
	}
	public void addJudgement(Judgement judge){
		if(judgements == null)
			judgements = new ArrayList<Judgement>();
		judgements.add(judge);
	}
}
