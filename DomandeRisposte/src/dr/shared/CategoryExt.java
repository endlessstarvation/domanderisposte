package dr.shared;
/*
 * L'oggetto CategoryExt rappresenta una versione estesa di Category. Contiene oltre agli string path ereditati, anche
 * gli arraylist di ID per risalire o scendere la struttura.
 * Questo oggetto NON viene scambiato fra Client e Server. Serve solo server-side per effettuare modifiche sul database.
 */
import java.util.ArrayList;
import java.util.Collections;

public class CategoryExt extends Category {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<Integer> SubcategoriesIds;
	private ArrayList<Integer> questionsIds;
	private int fatherId;
	
	//GET Statements
	public ArrayList<Integer> getSubcategoriesIds() {
		return SubcategoriesIds;
	}
	public ArrayList<Integer> getQuestionsIds() {
		return questionsIds;
	}
	public boolean hasSubcat(){
		return SubcategoriesIds != null;
	}
	public int getFatherId() {
		return fatherId;
	}
	public boolean hasQuestions(){
		return questionsIds != null;
	}
	public CategoryExt(int id, String name) {
		super(id, name);
		questionsIds = new ArrayList<Integer>();
	}
	
	
	public void setFather(int fatherId) {
		this.fatherId = fatherId;
	}
	public void setName( String name ){
		this.name = name;
	}
	public void addQuestion( int id ){
		if( this.SubcategoriesIds == null){
			this.SubcategoriesIds = new ArrayList<Integer>();
		}
		questionsIds.add(0, id );
	}
	public void addSubcategory( int id){
		if( this.SubcategoriesIds == null){
			this.SubcategoriesIds = new ArrayList<Integer>();
		}
		SubcategoriesIds.add( id );
	}
	/*
	 * Binary search question with id questionId and removes it.
	 */
	public void deleteQuestion(int questionId){
		int toDeleteIndex = Collections.binarySearch( getQuestionsIds(), questionId);
		if( toDeleteIndex >= 0){
			getQuestionsIds().remove( toDeleteIndex );
		}
	}
}
