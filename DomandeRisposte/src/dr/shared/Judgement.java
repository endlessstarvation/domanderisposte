package dr.shared;

import java.io.Serializable;
/*
 *  L'oggetto Judgement rappresenta un giudizio appartenente ad una risposta.
 */
public class Judgement implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String author;
	private int val;
	private int id;
	
	public Judgement(){}
	public Judgement( int id, String author, int val ){
		this.id = id;
		this.author = author;
		this.val = val;
	}
	
	//GET Statements
	public int getId() {
		return id;
	}
	public int getVal(){
		return val;
	}
	public String getAuthor(){
		return author;
	}
}
