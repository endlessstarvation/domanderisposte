package dr.shared;

/*
 * L'oggetto Category rappresenta una versione minimizzata contentene solo gli StringPath.
 * Questo oggetto viene scambiato fra Client e Server.
 */

import java.io.Serializable;
import java.util.ArrayList;

public class Category implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected int id;
	protected String name;
	private ArrayList<Category> subcategories;
	protected Category father;
	
	public Category(){}
	public Category( int id, String name ){
		this.id = id;
		this.name = name;
	}
	
	//GET Statements
	public int getId(){
		return this.id;
	}
	public Category getFather() {
		return father;
	}
	//Ritorna il nome della categoria
	public String getName(){
		return name;
	}
	public boolean hasFather(){
		return this.father != null;
	}
	public ArrayList<Category> getSubcategories(){
		return subcategories;
	}
	
	//Se la categoria non ha ancora sottocategorie, inizializzo la lista e aggiungo la entry.
	public void addSubcategory( Category c ){
		if(subcategories == null)
			subcategories = new ArrayList<Category>();
		subcategories.add(c);
		c.setFather(this);
	}
	public void setFather(Category father) {
		this.father = father;
	}
}
