package dr.server;
import java.util.concurrent.ConcurrentMap;
import org.mapdb.Atomic;
import org.mapdb.DB;
import dr.shared.Category;
import dr.shared.CategoryExt;
/*
 * Gestisce le operazioni sulle categorie che agiscono sulla mappa categoriesExt
 */
public class CategoryServiceImpl{
	// Database
	private DB db;
	public Atomic.Integer categoryIdGenerator;
	public ConcurrentMap<Integer, CategoryExt> categoriesExt;
	
	private int categoryRootId;
	
	public CategoryServiceImpl( DB db ) {
		this.db = db;
		this.categoryIdGenerator = this.db.getAtomicInteger( "categoryId" );
		this.categoriesExt = this.db.getTreeMap( "categories" );
		
		if( categoryIdGenerator.get() == 0 ){
			init();
		}
	}
	
	public void init(){
		CategoryExt cat = new CategoryExt( categoryIdGenerator.getAndIncrement(), "Root" );
		categoriesExt.put( cat.getId(), cat );
		categoryRootId = 0;
		
		addCategory( "Ambiente" );
		addCategory( "Animali" );
		addCategory( "Arte e cultura" );
		addCategory( "Elettronica e tecnologia" );
		addCategory( "Sport" );
		addCategory( "Svago" );
		this.db.commit();
	}
	
	// Azzera la collezione di Categorie
	public void clear(){
		this.categoryIdGenerator.set( 0 );
		this.categoriesExt.clear();
	}
	
	// Aggiunge una categoria al livello root
	public Void addCategory( String name ){
		return addCategory( this.categoryRootId, name );
	}
	
	// Aggiunge una categoria come sottocategoria di quella con id fatherId
	public Void addCategory( int fatherId, String name ){
		
		// Crea nuova categoria, setta il padre e la aggiunge al db
		int id = categoryIdGenerator.getAndIncrement();
		//this.db.commit();
		CategoryExt cat = new CategoryExt( id, name );
		cat.setFather( fatherId );
		this.categoriesExt.put( id, cat );
		
		// Aggiunge la categoria all'arrayList di sottocategorie del padre e aggiorna il db
		CategoryExt father = this.categoriesExt.get( fatherId );
		father.addSubcategory( cat.getId() );
		this.categoriesExt.put( fatherId, father );
		
		this.db.commit();
		return null;
	}
	
	public CategoryExt getCategory( int id){
		return categoriesExt.get( id );
	}
	
	// Ritorna la radice dell albero delle categorie
	public CategoryExt getCategoryRoot(){
		return categoriesExt.get( 0 );
	}
	
	// Ricostruisce ricorsivamente e ritorna l'albero delle categorie con oggetti Category 
	public Category getCategoryTree(){
		Category root = new Category( 0, "Categorie" );
		CategoryExt rootExt = this.getCategoryRoot();
		return getSubs( rootExt, root );
	}
	
	public Category getSubs( CategoryExt rootExt, Category rootCat ){
		if( rootExt.hasSubcat() ){
			for( int i = 0 ; i < rootExt.getSubcategoriesIds().size(); i++ ){
				CategoryExt curCatExt = getCategory( rootExt.getSubcategoriesIds().get( i ) );
				Category c = new Category( curCatExt.getId(), curCatExt.getName() );
				rootCat.addSubcategory( c );
				getSubs( curCatExt, c );
			}
		}
		return rootCat;
	}
	
	// Rinomina una categoria
	public Void renameCategory( int categoryId, String newName ){
		CategoryExt toRename = this.categoriesExt.get( categoryId );
		toRename.setName( newName );
		this.categoriesExt.put( categoryId, toRename );
		return null;
	}
}
