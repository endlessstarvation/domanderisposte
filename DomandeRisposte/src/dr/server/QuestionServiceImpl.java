package dr.server;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import org.mapdb.Atomic;
import org.mapdb.DB;

import dr.shared.Answer;
import dr.shared.CategoryExt;
import dr.shared.Judgement;
import dr.shared.Question;
import dr.shared.QuestionExtended;
/*
 * Gestisce le operazioni su domande, risposte e giudizi
 */
public class QuestionServiceImpl{
	// Database
	private DB db;
	private Atomic.Integer questionIdGenerator;
	private Atomic.Integer answerIdGenerator;
	private Atomic.Integer judgementIdGenerator;
	public Map<Integer, QuestionExtended> questionsExt;
	private Map<Integer, CategoryExt> categoriesExt;
	private Map<Integer, Judgement> judgements;
	
	private int PAGELENGTH = 30;
	
	public QuestionServiceImpl( DB db ){
		this.db = db;
		this.questionIdGenerator = this.db.getAtomicInteger("questionId");
		this.questionsExt = this.db.getTreeMap("questionsExt");
		this.categoriesExt = this.db.getTreeMap("categories");
		this.judgements = this.db.getTreeMap("judgements");
		this.judgementIdGenerator = this.db.getAtomicInteger("judgementId");
		this.answerIdGenerator = this.db.getAtomicInteger("answerId");
	}
	
	// Azzera tutte le domade
	public void clear() {
		this.questionsExt.clear();
		this.questionIdGenerator.set( 0 );
		this.judgements.clear();
		this.judgementIdGenerator.set( 0 );
		this.answerIdGenerator.set( 0 );
	}
	
	// Aggiunge una domanda
	public Void addQuestion( String text, String author, int categoryId){
		// Aggiunge la domanda al db
		int id = questionIdGenerator.getAndIncrement();
		Date date = new Date();
		QuestionExtended quest = new QuestionExtended( id, text, author, null, date, categoryId);
		this.questionsExt.put(id, quest);
		
		// Aggiunge la domanda alla categoria
		CategoryExt cat = this.categoriesExt.get( categoryId );
		cat.addQuestion( id );
		this.categoriesExt.put( categoryId, cat );
		while( this.categoriesExt.get( cat.getFatherId() ).getId() != 0 ){
			cat = this.categoriesExt.get( cat.getFatherId() );
			cat.addQuestion(id);
			this.categoriesExt.put( cat.getId(), cat );
		}
		this.db.commit();
		return null;
	}
	
	 /**
	 * Ricostruisce il percorso delle categorie di una domanda,
	 * utile per mostrare i breadcrumbs (i nomi dell categorie potrebbero cambiare)
	 */
	public void reconstructCategoryPath( QuestionExtended quest ){
		String path = "";
		CategoryExt cat = this.categoriesExt.get( quest.getCategoryId());
		path = cat.getName();
		System.out.println("Path 1. "+path);
		while( cat.getFatherId() != 0 ){
			cat = this.categoriesExt.get( cat.getFatherId());
			path = cat.getName()+">"+path;
		}
		System.out.println("path: "+path);
		quest.setCategoryPath( path );
	}
	
	// Restituisce il testo delle domande, da mostrare in home
	ArrayList<Question>getAllQuestions( ){
		int lastKey = questionIdGenerator.get() - 1;
		return getAllQuestions( lastKey );
	}
	
	// Restituisce tutte le domande a pagine di 30 elementi
	ArrayList<Question>getAllQuestions( int firstKey ){
		int currKey = firstKey;
		ArrayList<Question> result = new ArrayList<Question>();

		while( result.size() < PAGELENGTH && currKey >= 0 ){
			if( this.questionsExt.containsKey( currKey ) ){
				int id = this.questionsExt.get( currKey ).getId();
				String text = this.questionsExt.get( currKey ).getText();
				result.add( new Question( id,text ) );
			}
			currKey--;
		}
		return result;
	}
	
	// Restituisce tutte le domande di una categoria
	ArrayList<Question>getAllQuestionsByCategory( int categoryId ){
		ArrayList<Question>result = new ArrayList<Question>();
		ArrayList<Integer> questionsIds = this.categoriesExt.get( categoryId ).getQuestionsIds();
		for ( int i = 0; i < questionsIds.size(); i ++ ){
			QuestionExtended questExt = this.questionsExt.get(questionsIds.get(i));
			result.add( new Question( questExt.getId(), questExt.getText() ));
		}
		return result;
	}
	
	// Restituisce l'oggetto QuestionExtended ovvero la domanda completa di risposte e giudizi
	public QuestionExtended getFullQuestion( int id ){
		QuestionExtended result = this.questionsExt.get( id );
		reconstructCategoryPath( result );
		return result;
	}
	
	// Aggiunge una risposta a una domanda
	public Void addAnswer( String text, int questionId, String author, Date submissionDate ){
		QuestionExtended quest = this.questionsExt.get( questionId );
		Answer ans = new Answer( answerIdGenerator.getAndIncrement(), text, author, submissionDate);
		quest.addAnswer( ans );
		
		this.questionsExt.put( questionId, quest );
		this.db.commit();
		return null;
	}
	
	
	// Rimuove una risposta dall'oggetto domanda e dalla collezione di risposte
	public Void removeAnswer( int questionId, int answerId){
		QuestionExtended quest = this.questionsExt.get( questionId );
		quest.removeAnswer( answerId );
		this.questionsExt.put( quest.getId(), quest );
		this.db.commit();
		return null;
	}
	
	// Aggiunge un giudizio a una domanda
	public Void addJudgement( int questionId, int answerId, String author, int val){
		QuestionExtended quest = this.questionsExt.get( questionId );
		Judgement j = new Judgement( judgementIdGenerator.getAndIncrement(), author, val );
		quest.getAnswerById( answerId ).addJudgement( j );
		this.judgements.put(j.getId(), j);
		this.questionsExt.put( questionId, quest);
		this.db.commit();
		return null;
	}
	
	
	// Rimuove una domanda ( funzionalità admin )
	public Void removeQuestion( int questionId ){
		QuestionExtended quest = this.questionsExt.get( questionId );
		CategoryExt currCat = this.categoriesExt.get( quest.getCategoryId() );
		while( currCat.hasFather() ){
			currCat.deleteQuestion( questionId );
			currCat = this.categoriesExt.get( currCat.getFatherId() );
		}
		this.questionsExt.remove( questionId );
		this.db.commit();
		return null;
	}
	
}
