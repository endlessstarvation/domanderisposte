package dr.server;
import dr.shared.*;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentMap;
import org.mapdb.DB;

/*
 * Gestisce le operazioni sugli utenti
 */
public class LoginServiceImpl {
	
	// Database
	private DB db;
	// Collezione utenti
	private ConcurrentMap<String, User> userMap;
	
	public LoginServiceImpl( DB db){
		this.db = db;
		this.userMap = this.db.getTreeMap("user");
		
		if( userMap.get("admin") == null){ // l'admin deve essere sempre presente
			addAdmin();
		}
	}
	
	// Agigunge un utente al db, se già presente lancia eccezione 
	public User addUser( String username, String password, String email, String name, String surname, String sex, String birthDate, String birthPlace, String address ) throws DuplicatedUserNameException {
		if( userMap.get( username ) != null ){
			throw new DuplicatedUserNameException( username );
		}
		else{
			User user = new User(username,password,email,name,surname,sex,birthDate,birthPlace,address);
			user.setRoleRegistered(); //Setta il ruolo di default
			this.userMap.put( username, user );
			this.db.commit();
			return user;
		}
	}
	
	// Metodo che azzera la collezione utenti
	public void clear(){
		userMap.clear();
	}
	
	public void remove( String userName){
		userMap.remove( userName );
	}
	
	// Nomina un giudice settando il ruolo a 'j'
	public ArrayList<User> nominateJudge( String userName ){
		User judge = userMap.get( userName );
		judge.setRoleJudge();
		userMap.put(userName, judge);
		this.db.commit();
		return getRegistered();
	}
	
	public User downgradeJudge( String userName ){
		User user = userMap.get( userName );
		user.setRoleRegistered();
		userMap.put( userName, user );
		this.db.commit();
		return user;
	}
	
	// Aggiunge l'utente admin
	public void addAdmin(){
		User admin = new User("admin", "admin", "admin@admin.com", null, null, null, null, null, null);
		admin.setRoleAdmin();
		userMap.put("admin", admin);
		this.db.commit();
	}

	// Controlla username e password, se l'utente non è presente o la password è sbagliata lancia eccezioni
	public User login(String userName, String password) throws WrongPasswordException, UserNotFoundException {
		User user = this.userMap.get( userName );
		if( user != null ){
			if( !user.getPassword().equals(password) ){
				throw new WrongPasswordException();
			}
		}
		else{
			throw new UserNotFoundException();
		}
		return user;
	}
	
	// Tutti gli utenti ( funzionalità admin )
	public ArrayList<User> getAllUsers(){
		ArrayList<User> res = new ArrayList<User>(userMap.values());
		return res;
	}
	
	// Tutti gli utenti giudici ( funzionalità admin )
	public ArrayList<User> getJudges(){
		ArrayList<User> users = new ArrayList<User>( userMap.values() );
		ArrayList<User> res = new ArrayList<User>();
		for( User u : users){
			if( u.isJudge() ){
				res.add( u );
			}
		}
		res.trimToSize();
		return res;
	}
	
	// Tutti gli utenti non giudici o admin ( funzionalità admin )
	public ArrayList<User> getRegistered(){
		ArrayList<User> users = new ArrayList<User>( userMap.values() );
		ArrayList<User> res = new ArrayList<User>();
		for( User u : users ){
			if( u.isRegistered() ){
				res.add( u );
			}
		}
		res.trimToSize();
		return res;
	}
}
