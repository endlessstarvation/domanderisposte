package dr.server;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import dr.client.DRService;
import dr.shared.Category;
import dr.shared.DuplicatedUserNameException;
import dr.shared.Question;
import dr.shared.QuestionExtended;
import dr.shared.User;
import dr.shared.UserNotFoundException;
import dr.shared.WrongPasswordException;
/*
 * Smista le chiamate ai 3 Services, per non far instaziare 3 server al client
 * 
 * I metodi sono commentati nei 3 service
 */
public class DRServiceImpl extends RemoteServiceServlet implements DRService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	LoginServiceImpl loginService;
	QuestionServiceImpl questionService;
	CategoryServiceImpl categoryService;

	public DRServiceImpl() {
		DB db = DBMaker.newFileDB( new File( "DRDB.txt" ) ).closeOnJvmShutdown().make();

		loginService = new LoginServiceImpl( db );
		questionService = new QuestionServiceImpl( db );
		categoryService = new CategoryServiceImpl( db );
		
		// Per simulare.
		//restartDb(); // Drop del db
		//populate();
	}

	// DEBUG svuotare il db
	public void restartDb() {
		loginService.clear();
		categoryService.clear();
		questionService.clear();
		
		loginService.addAdmin();// Riaggiungo admin
		categoryService.init();// Riaggiungo categorie
	}

	// DEBUG popolare il db
	public void populate() {

		try {
			addUser( "mhack" , "m" , "margherita@hack.it" , null , null , null , null , null , null );
			addUser( "joob" , "m" , "john@noob.it" , null , null , null , null , null , null );
			User m = addUser( "massimo" , "massimo" , "massimo@mail.it" , null , null , null , null , null , null );
			m.setRoleJudge();
		} catch ( DuplicatedUserNameException e ) {
			e.printStackTrace();
		}

		addCategory( "Scienze" );
		addSubCategory( 1 , "Ecologia" );
		addSubCategory( 4 , "Programmazione" );
		addSubCategory( 9 , "JavaScript" );
		addSubCategory( 9 , "Java" );
		addSubCategory( 9 , "PHP" );

		String text = "Che differenza c'è tra Java e JavaScript?";
		String author = "John Noob";
		int categoryId = 10;

		String text2 = "Come si fa a far venire quelli di YouTube a filmarti? Ho provato provato provato a chiamare YouTube tutto il giorno perché venissero a filmarmi ma non mi hanno risposto. Come fanno le altre persone ad avere lì i loro video? Avrei della roba divertente ma non vengono.";
		String author2 = "admin";
		int categoryId2 = 4;

		String text3 = "Perchè le stelle viste con il telescopio non hanno 5 punte?";
		String author3 = "M. Hack";
		int categoryId3 = 7;

		addQuestion( text , author , categoryId );

		addQuestion( text2 , author2 , categoryId2 );

		addQuestion( text3 , author3 , categoryId3 );

		addAnswer( "Sono due linguaggi totalmente diversi" , 0 , "admin" , new Date() );

		addAnswer( "Risposta 2" , 0 , "M. Hack" , new Date() );

	}

	public User addUser( String username, String password, String email, String name, String surname, String sex,
			String birthDate, String birthPlace, String address ) throws DuplicatedUserNameException {
		return loginService.addUser( username , password , email , name , surname , sex , birthDate , birthPlace ,
				address );
	}

	public User login( String userName, String password ) throws WrongPasswordException, UserNotFoundException {
		return loginService.login( userName , password );
	}

	public ArrayList<User> getJudges() {
		return loginService.getJudges();
	}

	public ArrayList<User> getRegistered() {
		return loginService.getRegistered();
	}

	public ArrayList<User> getAllUsers() {
		return loginService.getAllUsers();
	}
	
	public ArrayList<User> nominateJudge( String userName ){
		return loginService.nominateJudge( userName );
	}

	public ArrayList<Question> addQuestion( String text, String author, int categoryId ) {
		questionService.addQuestion( text , author , categoryId );
		return getAllQuestions();
	}

	public ArrayList<Question> getAllQuestions() {
		return questionService.getAllQuestions();
	}

	public ArrayList<Question> getAllQuestions( int firstKey ) {
		return questionService.getAllQuestions( firstKey );
	}

	public QuestionExtended getFullQuestion( int questionId ) {
		return questionService.getFullQuestion( questionId );
	}

	public ArrayList<Question> getAllQuestionsByCategory( int categoryId ) {
		return questionService.getAllQuestionsByCategory( categoryId );
	}

	public QuestionExtended addJudgement( int questionId, int answerId, String author, int val ) {
		questionService.addJudgement( questionId , answerId , author , val );
		return getFullQuestion( questionId );
	}

	public QuestionExtended addAnswer( String text, int questionId, String author, Date submissionDate ) {
		questionService.addAnswer( text , questionId , author , submissionDate );
		return getFullQuestion( questionId );
	}

	public ArrayList<Question> removeQuestion( int questionId ) {
		questionService.removeQuestion( questionId );
		return getAllQuestions();
	}

	public QuestionExtended removeAnswer( int questionId, int answerId ) {
		questionService.removeAnswer( questionId , answerId );
		return getFullQuestion( questionId );
	}

	public Category getCategoryTree() {
		return categoryService.getCategoryTree();
	}

	public Category addCategory( String name ) {
		categoryService.addCategory( name );
		return getCategoryTree();
	}

	public Category addSubCategory( int fatherId, String name ) {
		categoryService.addCategory( fatherId , name );
		return getCategoryTree();
	}

	public Category renameCategory( int categoryId, String newName ) {
		categoryService.renameCategory( categoryId , newName );
		return getCategoryTree();
	}
}
